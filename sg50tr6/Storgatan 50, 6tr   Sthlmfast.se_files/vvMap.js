var vvMap = null;
var vvMapSettings = new Array();
template = 'site/_simma/';
vvMapSettings['_ClustererStyles'] = [{
	url: '/'+template+'images/map/cluster30.png',
	height: 83,
	width: 83,
	anchor: [0, 0],
	textColor: '#FFFFFF',
	textSize: 14
}, {
	url: '/'+template+'images/map/cluster40.png',
	height: 83,
	width: 83,
	anchor: [0, 0],
	textColor: '#FFFFFF',
	textSize: 14
}, {
	url: '/'+template+'images/map/cluster50.png',
	width: 83,
	height: 83,
	anchor: [0, 0],
	textColor: '#FFFFFF',
	textSize: 15
}];







function vvMapLoad() {	
	//vvDebugg('vvMap');
	if(vvMapSettings == null){
		//vvDebugg(vvMapSettings.length);
		vvMapClearSettings();
	}	
	if(typeof google == 'object' && google.maps != undefined){		
		//vvDebugg('_mapHolder check');
		if($(vvMapSettings['_mapHolder']).get(0) == undefined){			
			if(vvMapSettings['_mapTry']<=5){
				//vvDebugg(vvMapSettings['_mapHolder']+' - _mapHolder try again : '+vvMapSettings['_mapTry']);
				vvMapSettings['_mapTry']++;
				setTimeout('vvMapLoad()',100);
			}else{
				//vvDebugg(vvMapSettings['_mapHolder']+' - Found : '+vvMapSettings['_mapTry']);
			}
			return;
		} else {
			//vvDebugg('vvMap load');
		}
		if(vvMapSettings['_mapCenterSet'] == null){
			//vvDebugg('vvMap center general');
			vvMapSettings['_mapCenter'] = new google.maps.LatLng(55.677584411089505, 10.535888671875);			 
		}else{	
			temp = vvMapSettings['_mapCenterSet'].split(',');			 
			vvMapSettings['_mapCenter'] = new google.maps.LatLng(temp[0], temp[1]);			
		}
		 
		vvMap = new google.maps.Map($(vvMapSettings['_mapHolder']).get(0), {
			zoom: vvMapSettings['_zoom'],
			center: vvMapSettings['_mapCenter'],
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		//vvDebugg('vvMap loaded');
		vvLoadMapOptions();
	}else{
		/* Load google map v3 */
		//vvDebugg('Google not loaded. Reload in 100ms');
		
		setTimeout('vvMapLoad()',100);
	}
}

/*
 * vvLoadMapOptions()
 * 
*/ 
function vvLoadMapOptions(){	
	
	// Set options
	if(settings['_vvMapOptions'] != null){
		//vvDebugg(settings['_vvMapOptions']);
		vvMap.setOptions(settings['_vvMapOptions']);
	}
	
	if(vvMapSettings['_gps']){
		// Try W3C Geolocation (Preferred)
		if(navigator.geolocation) {			
			navigator.geolocation.getCurrentPosition(function(position) {
				initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				vvMap.panTo(initialLocation);
				vvMap.setZoom(14);
				var m = {};
				m.position = initialLocation;
				m.title = "Här är du!";
				userMark = new google.maps.Marker(m);
				userMark.setMap(vvMap);	
			});
		}
	}

	
	
 
 	
	/*
	 * If markers added from page, add to map
	 * marker = {center:'10.10,10.10',icon:''}
	 * 
	*/
	if(vvMapSettings['_markers'].length > 0){
		for(e in vvMapSettings['_markers']){
			marker = vvMapSettings['_markers'][e];
			if(marker.center != undefined){
				temp = marker.center.split(',');			
				marker.position = new google.maps.LatLng(temp[0], temp[1]);
				vvMarker = new google.maps.Marker(marker);
				vvMarker.setMap(vvMap);	
			}		
		}
	}
	
	// Add overlay, prevent scroll
	if(vvMapSettings['_mapOverlay']){
		$(vvMapSettings['_mapHolder']).append('<div class="_mapoverlay" title="Klicka för att aktivera" onclick="$(this).hide()" style="cursor:pointer;position:absolute;background:rgba(255,255,255,0.2);z-index:99999999999999;width:100%;height:100%;top:0px;left:0px;"></div>');
	}	
	 // Load maps event like click, bounds etc
	vvLoadMapEvents();
	
	// Load markers from data
	vvLoadMarkers();
	
}


/*
 * vvLoadMapEvents()
 * 
*/
function vvLoadMapEvents(){
	 // Click event
	 google.maps.event.addListener(vvMap, "click", function (e) {
		if(vvMapSettings['_mapMakePath']){	 			 
			vvAddMarkersToPath(e.latLng);
		}
		if(vvMapSettings['_mapAddMark']){
			vvAddMarkerToMap(e.latLng);
		}
	});	
	// idle, bounds change
	google.maps.event.addListener(vvMap, 'idle', function() {
		vvOnMapUpdate();
		setTimeout('vvOnMapUpdate()',200);
	}); 
}



/*
 * vvAddMarkerToMap()
 * 
*/
function vvAddMarkerToMap(latLng){
	
	if(vvMapSettings['_mapSingelMark'] != null){
		vvMapSettings['_mapSingelMark'].setMap(null);
		vvMapSettings['_mapSingelMark'] = null;
	}
	vvMapSettings['_mapSingelMark'] = new google.maps.Marker({
		position: latLng,
		draggable: true,
		title: i.toString(),
		map: vvMap,
		icon:new google.maps.MarkerImage('/'+template+'images/icons/add.png', new google.maps.Size(39, 45), new google.maps.Point(0, 0), new google.maps.Point(	19, 44))
	});
	
	$(document).trigger('_mapMarkSpot',vvMapSettings['_mapSingelMark'].getPosition());
	google.maps.event.addListener(vvMapSettings['_mapSingelMark'], 'dragend', function (event) {
		$(document).trigger('_mapMarkSpot',vvMapSettings['_mapSingelMark'].getPosition());
	});
}


/*
 * vvRemoveSingelMark()
 * 
*/
function vvRemoveSingelMark(){
	if(vvMapSettings['_mapSingelMark'] != null){
		vvMapSettings['_mapSingelMark'].setMap(null);
		vvMapSettings['_mapSingelMark'] = null;
	}
}


/*
 * vvCreatePoly()
 * Skapar polyline för att sedan kunna rita ut den
*/
function vvCreatePoly(){
	var polyOptions = {
		strokeColor: '#000000',
		strokeOpacity: 1.0,
		strokeWeight: 3,
		map: vvMap		
	};
	vvMapSettings['_mapPoly'] = new google.maps.Polyline(polyOptions);
}

/*
 * vvAddMarkersToPath()
 * Ropas när man klickar på kartan. 
 * Lägger till en marker i _mapPolyMarkers och lägger även till dragend
*/
function vvAddMarkersToPath(latLng){
	var i = vvMapSettings['_mapPolyMarkers'].length;
	i++;
	if(i==1){
		i = 0;
	}
	//vvDebugg(i);
	
	var marker = new google.maps.Marker({
		position: latLng,
		draggable: true,
		title: i.toString(),
		map: vvMap,
		icon:new google.maps.MarkerImage('/'+template+'images/icons/dot.png', new google.maps.Size(10, 10), new google.maps.Point(0, 2), new google.maps.Point(5, 5))
	});
	
	google.maps.event.addListener(marker, 'drag', function (event) {
		vvPaintPolyPath();
	});
	
	vvMapSettings['_mapPolyMarkers'].push(marker);
	marker = "";
	// Ritar ut polyline mellan vvMapSettings['_mapPolyMarkers']
	vvPaintPolyPath();
}


/*
 * vvPaintPolyPath()
 * Ropas när man klickar på kartan. 
 * Lägger till en marker i _mapPolyMarkers och lägger även till dragend
*/
function vvPaintPolyPath(){
	if(vvMapSettings['_mapPoly'] != null){
		vvMapSettings['_mapPoly'].setMap(null);
		vvMapSettings['_mapPoly'] = null;
	}
	// Skapar polyline
	vvCreatePoly();
	var i = 0;
	// Skapar en path
	vvMapSettings['_mapPath'] = new Array();	
	for(var e in vvMapSettings['_mapPolyMarkers']){
		if(i==0){
			$(document).trigger('_mapMarkSpot',vvMapSettings['_mapPolyMarkers'][e].getPosition());
		}
		i++;
		vvMapSettings['_mapPath'].push(vvMapSettings['_mapPolyMarkers'][e].getPosition());
	}
	// lägg till pathen i polyline
	vvMapSettings['_mapPoly'].setPath(vvMapSettings['_mapPath']);
	
	// Skapa en encoded version av pathen
	var path = google.maps.geometry.encoding.encodePath(vvMapSettings['_mapPoly'].getPath());
	$(document).trigger('_mapPolyPath',path);
}

/*
 * vvClearPolyPath()
 * För att återsätlla en rute
*/
function vvClearPolyPath(){
	if(vvMapSettings['_mapPoly'] != null){
		vvMapSettings['_mapPoly'].setMap(null);
		vvMapSettings['_mapPoly'] = null;
	}
	vvMapSettings['_mapPath'] = [];
	
	for(var e in vvMapSettings['_mapPolyMarkers']){
		vvMapSettings['_mapPolyMarkers'][e].setMap(null);
	}
	vvMapSettings['_mapPolyMarkers'] = [];
	//vvDebugg(vvMapSettings['_mapPolyMarkers'].length);
	
	//vvDebugg('Clear poly path');
}



/*
 * vvLoadMarkers()
 * 
 *
*/
function vvLoadMarkers(){
	//vvDebugg('loadMarkers');
	if(vvMapSettings['_markersBoxLoad']){
		infobox = new InfoBox({
			 content: '<div id="infoBox"></div>',
			 disableAutoPan: false,
			 maxWidth: 150,
			 maxHeight:94,
			 pixelOffset: new google.maps.Size(-110, 0),
			 zIndex: null,
			 boxStyle: {			   
				height:"115px",
				width: "300px"
			},
			closeBoxMargin: "",
			closeBoxURL: "/"+template+"images/close.png",
			infoBoxClearance: new google.maps.Size(1, 1)
		});
		//vvDebugg('vvLoadShow');
		vvLoadShow();
		if(mapdata == undefined){		
			vvLoadHide();
			//vvDebugg('No map data');
		}else{
			vvLoadHide();	
			vvMapSettings['_mapData'] = mapdata;		
			vvAddMarkersToMap();
		}
	}
}

function vvAddMarkersToMap(){
	//vvDebugg('vvAddMarkersToMap');
	//Reset
	if(vvMapSettings['_markerClusterer'] != null){
		//vvDebugg('clearMarkers');
		vvMapSettings['_markerClusterer'].clearMarkers();
	}
		 
	//Populate map
	i = 0;
	for (d in vvMapSettings['_mapData']) {
		var spot = vvMapSettings['_mapData'][d];	
		
		spot.vvShow = vvMapSettings['_markersHideOnStart'];
		//spot.image = template+'images/icons/'+spot.cat+'.png';
		spot.image = new google.maps.MarkerImage('/'+template+'images/icons/pins_60px.png', new google.maps.Size(51, 51), new google.maps.Point(spot.pin_x, spot.pin_y));
		//MakeMarker
		vvParseMarker(spot,i);
		i++					
	}
	
	vvMapSettings['_markerClusterer'] = new MarkerClusterer(vvMap, vvMapSettings['_markers'], {
	  styles: vvMapSettings['_ClustererStyles'],
	  gridSize: 50
	});
	
	
	
}


function vvMapShowMarkers(){
	for (var it in vvMapSettings['_markers']) {
		if(!IE){
			vvMapSettings['_markers'][it].setVisible(true);
		}else{
			 
		}
	}			
	vvMapSettings['_markerClusterer'].repaint();
}

function vvMapHideMarkers(){
	
	
	
	for (var it in vvMapSettings['_markers']) {
		if(!IE){vvMapSettings['_markers'][it].setVisible(false);}else{
			 
		}
	}			
	vvMapSettings['_markerClusterer'].repaint();
	
}

function vvRemoveMarkers(){
	$(vvMapSettings['_markers']).each(function(index, o) {
		if(o != undefined){
			//vvDebugg(o);
			//o.setMap(null);
		}
	});
}

function vvParseMarker(spot,i){
	
	var latLng = new google.maps.LatLng(spot.lat,spot.lng);
	var im = spot.image;
	
	markerHtml = '<a id="infoBox" sid="'+spot.id+'" title="'+spot.title+'" href="'+path+'spot/'+spot.url+'/"><div class="infoBox-img dib">'+im+'</div><div class="dib">';
	markerHtml += '<div class="infoBox-title">'+ vvEscapeHTMLEncode(spot.title) +'</div>';
	markerHtml += '<div class="infoBox-opt small">';
	for(k in spot){
		if(k.match(/opt_/)){
			if(spot[k] == '1'){
				markerHtml += '<div class="icon '+k+'"></div>';
			}		
		}
	}	
	markerHtml += '</div>';
	markerHtml += '</div></a>';
	var swimup = spot.swimup;


	var marker = new google.maps.Marker({
		position: latLng,
		icon: spot.image,
		html:markerHtml,
		title:spot.title,
		url:spot.url,
		cat:spot.cat,
		opt:spot.opt,
		id:spot.id,
		type:spot.type,
		type_string:spot.type_string,
		pin_x:spot.pin_x,
		pin_y:spot.pin_y,
		swimup:swimup,
		vvShow:spot.vvShow
	});
	vvMapSettings['_markers'][i] = marker;
	
	if(vvMapSettings['_markerInfobox']){
		google.maps.event.addListener(vvMapSettings['_markers'][i], 'click', function() {
			vvMap.panTo(this.getPosition());
			infobox.setContent(this.html);
			infobox.open(vvMap,this);
			//vvDebugg('click');
			setTimeout(function(){
				$(document).trigger('vvInit');
				$(".infoBox-title").click(function(e) {
				   /*
					var title  = $(this).parent().parent().attr('title')+' | Spot';
					var url = path+'spot/'+$(this).parent().parent().attr('url')+'/';
					
					pushState({eID:"",eTitle:title}, title, url);	
					*/
				});
			},200);		
		});
	}else{
		google.maps.event.addListener(vvMapSettings['_markers'][i], 'click', function() {
			vvMapSettings['_markerClickFunction'](this);
			vvMap.panTo(this.getPosition());
			vvMap.setZoom(16);			
			//vvDebugg('click');
			setTimeout(function(){
				$(document).trigger('vvInit');				
			},200);	
				
		});
	}
}


function vvEscapeHTMLEncode(str) {
	if (typeof(str) == "string") {
	  str = str.replace(/&gt;/ig, ">");
	  str = str.replace(/&lt;/ig, "<");
	  str = str.replace(/&#039;/g, "'");
	  str = str.replace(/&quot;/ig, '"');
	  str = str.replace(/&amp;/ig, '&'); /* must do &amp; last */
	  }
	 return str;
}


function vvMapClearSettings(){	 
	if(vvMapSettings == null){
		vvMapSettings = new Array();
	}
	//vvDebugg('_map clear setteing');
	vvMapSettings['_mapTry'] = 0;	
	vvMapSettings['_mapHolder'] = '._map';	
	
	vvMapSettings['_mapStyles'] = [];
	
	vvMapSettings['_mapCenter'] = null; 
	vvMapSettings['_mapCenterSet'] = null;	
	vvMapSettings['_show_Fullscreen'] = false;
	vvMapSettings['_markers'] = new Array();
	vvMapSettings['_markersInCluster'] = new Array();
	vvMapSettings['_vvMapOptions'] = {};
	vvMapSettings['_zoom'] = 8;	
	
	vvMapSettings['_gps'] = false;
	
	
	vvMapSettings['_mapOverlay'] = false;	
	
	vvMapSettings['_markersHideOnStart'] = false;
	
	vvMapSettings['_markerClickFunction'] = function(data){
		//vvDebugg('Clicker');
	};
	
	vvMapSettings['_mapMakePath'] = false;
	vvMapSettings['_mapAddMark'] = false;
	vvMapSettings['_mapSingelMark'] = null;
	
	
	vvMapSettings['_mapPath'] = new Array();
	vvMapSettings['_mapPoly'] = null;
	vvMapSettings['_mapPolyLinesMarkOnMap'] = true;
	vvMapSettings['_mapPolyLineStrokeWeight'] = 2;
	vvMapSettings['_mapPolyLines'] = new Array();
	vvMapSettings['_mapPolyLinesMarkers'] = new Array();
	
	vvMapSettings['_mapTempPath'] = null;
	
	vvMapSettings['_mapPolyMarkers'] = new Array();
	vvMapSettings['_mapShowPolyMarkers'] = true;
	
	vvMapSettings['_markerSelected'] = null;
	vvMapSettings['_markerInfobox'] = false;
	vvMapSettings['_markersBoxLoad'] = false;
	vvMapSettings['_markerLetters'] = false;
	vvMapSettings['_markersInfoHolder'] = '#map_info'; 
	vvMapSettings['_markerClusterer'] = null;	
	vvMapSettings['_markerIcon'] = new Array(42,43);	
	vvMapSettings['_markHolder'] = "#_markHolder";
	
	vvMapSettings['_markList'] = new Array();
	
	//abcdefghijklmnopqrstuvwxyzåäö
	vvMapSettings['_abc'] = new Array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','å','ä','ö');	
	
}

function vvMapUpdate(){
	temp = vvMapSettings['_mapCenterSet'].split(',');			 
	vvMap.setCenter(vvMapSettings['_mapCenter']);
}

function vvOnMapUpdate(){
	
	
	if(vvMapSettings['_markerLetters']){	
		var l = 0;	 
		$(vvMapSettings['_markersInfoHolder']).html('');
		$("._mapListArr").html('');
		for (var i=0; i<vvMapSettings['_markers'].length; i++){
			mark = vvMapSettings['_markers'][i];			
			 			
			y = mark.pin_y
			mark.favv = '';	
			if(inArray(mark.id,vvMapSettings['_markList'])){
				mark.favv = '_favv';					
				$("._mapListArr").append(vvSpotMarkup(l,mark));											
				if(mark.swimup.length>0){
					y = 179;
				}else{
					y = 140;
				}
			}		 
			
			if( vvMap.getBounds().contains(vvMapSettings['_markers'][i].getPosition()) ){
				if(mark.getMap() != null){					
					var newImge = new google.maps.MarkerImage('/'+template+'images/icons/circles_abc50.png', new google.maps.Size(vvMapSettings['_markerIcon'][0], vvMapSettings['_markerIcon'][1]), new google.maps.Point(l*vvMapSettings['_markerIcon'][0],y),new google.maps.Point(20,20));
					mark.setIcon(newImge);
					mark.l = l;
					$(vvMapSettings['_markersInfoHolder']).append(vvSpotMarkup(l,mark));
					l++;
				}
			}
		}
		$(document).trigger('vvInit');
	}
	
}



function vvSpotMarkup(l,mark){
	var html = $(vvMapSettings['_markHolder']).html();
	var pin_x = (l*vvMapSettings['_markerIcon'][0]);	
	html = html.replace(/\{letter\}/g, l);
	
	html = html.replace(/\{pin_x\}/g, pin_x);	
	if(vvMapSettings['_markerSelected'] == mark.id){
		html = html.replace(/\{item_class\}/g, 'aktiv');
	}else{
		html = html.replace(/\{item_class\}/g, '');
	}
				
	for (var key in mark) {	 
		
		var val = mark[key];
		if(val != undefined){
			if(typeof val != 'function'){
				if(key == 'swimup'){
					
					var swimupHtml = "";
					
					if(val.length>0){
						
						for(var s in val){
							swimup = val[s];
							var swimupTemp = $("#_swimup").html();					
							for(var key_u in swimup){
									
								var val_u = swimup[key_u];	
												
								var myString = "\{"+key_u+"\}";		 
								var regExp = new RegExp(myString,'g');
								swimupTemp = swimupTemp.replace(regExp, val_u);	
							}
							swimupHtml = swimupHtml+swimupTemp;
						}
					}
					
					var myString = "\{swimups\}";		 
					var regExp = new RegExp(myString,'g');
					html = html.replace(regExp, swimupHtml);
					
				}else{
					if(typeof val != 'object'){
						var myString = "\{"+key+"\}";		 
						var regExp = new RegExp(myString,'g');
						html = html.replace(regExp, val); 
					}
				}
				
			}
		}
	}  	
	return html;
}



function decodeLevels(encodedLevelsString) {
    var decodedLevels = [];
    for (var i = 0; i < encodedLevelsString.length; ++i) {
        var level = encodedLevelsString.charCodeAt(i) - 63;
        decodedLevels.push(level);
    }
    return decodedLevels;
}

function vvMapRemoveRutes(){
	for (var it in vvMapSettings['_mapPolyLines']) {		
		vvMapSettings['_mapPolyLines'][it].setMap(null);
	}
	for (var it in vvMapSettings['_mapPolyLinesMarkers']) {		
		vvMapSettings['_mapPolyLinesMarkers'][it].setMap(null);
	}
}

function vvPaintRute(pathString,color,antal,save,dot){
	
	dot = typeof dot !== 'undefined' ? dot : false;
	save = typeof save !== 'undefined' ? save : true;
	color = typeof color !== 'undefined' ? color : '#FF0000';
	
	var tempPath = google.maps.geometry.encoding.decodePath(pathString);
	antal = typeof antal !== 'undefined' ? antal : tempPath.length;

	var decodedLevels = decodeLevels("BBBBBBBBB");
	var distance = google.maps.geometry.spherical.computeLength(tempPath);
	var path = new Array();
	
	
	if(vvMapSettings['_mapShowPolyMarkers']){
		var p = 0;
		for(var e in tempPath){				
			if(p<=antal){				
				path.push( tempPath[e] );
			}				
			if(p == 0 || (p+1) == tempPath.length){
				img = new google.maps.MarkerImage('/'+template+'images/icons/dot.png', new google.maps.Size(10, 10), new google.maps.Point(0, 17), new google.maps.Point(5, 5));
			}else{
				img = new google.maps.MarkerImage('/'+template+'images/icons/dot.png', new google.maps.Size(10, 10), new google.maps.Point(0, 2), new google.maps.Point(5, 5));
			}
			if(dot != false && (p) == dot){
				img = new google.maps.MarkerImage('/'+template+'images/icons/add.png', new google.maps.Size(39, 45), new google.maps.Point(0, 0), new google.maps.Point(19, 44));				
				vvMapSettings['_mapPolyLinesMarkers'].push(new google.maps.Marker({
					position: tempPath[e],
					draggable: false,
					map: vvMap,
					icon:img
				}));
			}
			p++;
			
			
			if(vvMapSettings['_mapPolyLinesMarkOnMap']){
				vvMapSettings['_mapPolyLinesMarkers'].push(new google.maps.Marker({
					position: tempPath[e],
					draggable: false,
					map: vvMap,
					icon:img
				}));
			}	 
		}
	}
	
	if(!save){
		if(vvMapSettings['_mapTempPath'] != null){
			vvMapSettings['_mapTempPath'].setMap(null);
			vvMapSettings['_mapTempPath'] = null;
		}
		
		vvMapSettings['_mapTempPath'] = new google.maps.Polyline({
			path: path,
			geodesic: true,			
			icon: {
			  path: google.maps.SymbolPath.CIRCLE,
			  scale: 2
			},			
			strokeColor: color,
			strokeOpacity: 0.9,
			strokeWeight: vvMapSettings['_mapPolyLineStrokeWeight'],			
			map:vvMap
		});
	}else{
		
		vvMapSettings['_mapPolyLines'].push(new google.maps.Polyline({
			path: path,
			geodesic: true,			
			icon: {
			  path: google.maps.SymbolPath.CIRCLE,
			  scale: 2
			},			
			strokeColor: color,
			strokeOpacity: 0.9,
			strokeWeight: vvMapSettings['_mapPolyLineStrokeWeight'],			
			map:vvMap
		}));
		
	}
	
	
	return {distance:distance,path:path};
}


