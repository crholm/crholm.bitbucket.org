var rrSharon = false;

(function($) {	
/**
 * 
 * $("#test").();
 * $("#test").data('sharon').Open();
 */
$.sharon = function(el,op) {
	var defaults = {
		offset: {"left": 0, "top": 0},
		c: "shareBox",
		facebook: "https://www.facebook.com/sharer/sharer.php?u=",
		twitter: "https://twitter.com/share?url=",
		google: "https://plus.google.com/share?url=",
		popupOptions: "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=600",
		icons: {
			"facebook": "/site/_jimbeam/images/facebook.png",
			"twitter": "/site/_jimbeam/images/twitter.png",
			"google": "/site/_jimbeam/images/google.png"
		}
	}	
	
	var p = this;
	var obj = $(el);
	
	p.init = function() {
		/*
		if ($(this).data('test')) {				
		  	return (false);
		} else {
			p.settings = {};
			$(this).data('test', true);		
		}*/	
		
	  	p.settings = $.extend({}, defaults, op);			
	  	p.vvSetup();
	}
	
	p.vvSetup = function() {
		
		/* EDIT --- SÅ den kör docuemnt bind click bara 1 gång */
		if(!rrSharon){
			rrSharon = true;
			$(document).bind("click", function (e) {
				
				var fade = true;
				if (undefined !== e.target) {
					if ($(e.target).hasClass(p.settings.c)) {
						fade = false;
					} else {
						/* Check parents. */
						if (undefined !== e.toElement.offsetParent) {
							if ($(e.toElement.offsetParent).hasClass(p.settings.c)) {
								fade = false;
							} else {
								fade = true;
							}
						}
					}
				}
				 
				/* Fade. */
				if (fade) {					
					$("." + p.settings.c).fadeOut();
				}
	
			});
		}
		//vvDebugg('sharon');
		/* Now, rebind the click event for this handle. */
		obj.unbind("click").bind("click", function (e) {
			e.preventDefault();

			/* Fade out and delete any remaining sharons. */
			$("." + p.settings.c).remove();

			/* Construct the URLs. */
			var urls = p.constructUrl($(this).attr("href"));

			/* Create the position element. */
			var x = e.pageX + p.settings.offset.left;
			var y = e.pageY + p.settings.offset.top;
			var position = "position: absolute; top: "+ y +"px; left: "+ x +"px";

			/* Create the skeleton div. */
			var skeleton = $("<div/>", {'class': p.settings.c, style: position});

			/* Create the javascript. */
			var javascript = "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,";
			javascript += "resizable=yes,scrollbars=yes,height=300,width=600');return false;"		
			/* Create URLs. */
			var facebookTag = $("<a/>", {c: "sharonFacebook", href: urls.facebook,onclick: javascript, target: "_blank"});
			var twitterTag = $("<a/>", {c: "sharonTwitter", href: urls.twitter, 
			    onclick: javascript, target: "_blank"});
			var googleTag = $("<a/>", {c: "sharonGoogle", href: urls.google, 
			    onclick: javascript, target: "_blank"});

			/* Add icons to tags. */
			facebookTag.html("<img src='" + p.settings.icons.facebook +"' />");
			twitterTag.html("<img src='" + p.settings.icons.twitter +"' />");
			googleTag.html("<img src='" + p.settings.icons.google +"' />");

			/* Add tags to skeleton. */
			skeleton.append(facebookTag).append(twitterTag).append(googleTag);

			/* Add skeleton to document. */
			$("body").append(skeleton);

			/* Make it fade in. */
			skeleton.fadeIn();
			
			return (false);
		});
	};

	p.constructUrl = function (link) {
		var urls = {
			"facebook": p.settings.facebook + link,
			"twitter": p.settings.twitter + link,
			"google": p.settings.google + link 
		}

		return (urls);
	}

	p.init();	
}
$.fn.sharon = function(options) {							
	return this.each(function() {		 
		if (undefined == $(this).data('sharon')) {
			var plugin = new $.sharon(this, options);
			$(this).data('sharon', plugin);
		}else{
		
		}
	});							
}
})(jQuery);


/**
 * vvStars
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Detta är gjort för sourz
 *
 */

$.fn.vvStars = function(options){	
	var defaults = {
		parent	:	null,
		url		: 	"/?init=vvVote&vvAjaxRequest=true",		
		callback: 	null,
		c	: '_star',
		stars	: 5,
		parms	: {},
		idx		: 0,
		type	: 'star',
		rate	: 0,
		o		: false,
		start	: false,
		sending	: false
		
	}							
	var p = this;								
	p.settings = {};
								
	p.init = function() {
		
		if($(this).data('test')){
		  return false;
		}
		$(this).data('test',true);
		
	  	p.settings = $.extend({}, defaults, options);			
	  	p.vvSetup();
	  	$(p).mouseleave(function(e) {
		  	p.vvUpdate(p.settings.rate);
	  	}); 
	}
	p.callback = function(data){
	  	vvDebugg(data);
		if(data.error == true){
			if(data.fb == false){
				if(confirm(data.fbmsg)){
					top.location =  htmlspecialchars_decode(data.fburl) ;
				}
			}
			if(data.action == "msg"){
				alert(data.msg);
			}
			return;
		}else{
			
		}
		if(data.success){
			p.vvOk();
		}else{
			p.vvDoh();
		}
		p.settings.rate = data.rate;	
		if(data.votes == 1){
			var t = data.rate+' of '+data.votes+' vote';
		}else{
			var t = data.rate+' of '+data.votes+' votes';
		}
		$(p).find('._rate_info').html(t);
		p.settings.u = true;
		p.vvUpdate(0);	
	};	
	p.vvSetup = function() {
		i = 0;		 
		while(i<p.settings.stars){			
			i++;
			var obj = $('<div class="'+p.settings.c+' _star-'+i+'" rate="'+i+'" id="_star_'+p.settings.idx+'_'+i+'">&nbsp;</div>');
			obj.click(function(e){
				e.preventDefault();
				if(!p.settings.start){
					p.vvVote($(this).attr('rate'));				
				}
			});			
			obj.mouseenter(function(e) {
				p.vvUpdate($(this).attr('rate'));				
			});			
			$(p).append(obj);
		}
		var t = $(p).attr('votes');
		var m = $(p).attr('rate');
		if(t == 0){
			var t = t+' votes';
		}else if(t == 1){
			var t = m+' of '+t+' vote';
		}else{
			var t = m+' of '+t+' votes';
		}
		var obj = $('<div class="_rate_info " id="_info_'+p.settings.idx+'_'+i+'">'+t+'</div>');
		$(p).append(obj);
		p.vvUpdate(p.settings.rate);
					
	};
	p.vvOk = function() {
		$(p).fadeTo(100,0.5).delay(500).fadeTo(100,0.9);
	}
	p.vvDoh = function() {
		$(p).fadeTo(100,1);
	}
	p.vvUpdate = function(o) {
		if(p.settings.sending){
			return;
		}
		if(p.settings.u){
			o = p.settings.rate;
		}
		g = parseFloat(o)+1;
		d = Math.floor((g - Math.floor(g))*10);
		i = parseInt(g);
		
		if(d>5){
			h = true; 		 
		}else{			 
			h = false;
		}		
		
		$(p).find('._star').removeClass('_a');
		$(p).find('._star').removeClass('_b');
		if(h){
			$(p).find('._star-'+i).addClass('_b');
		}else{
			
		}
		while(i>0){
			i--;
			$(p).find('._star-'+i).addClass('_a');			
		}
	};
	p.vvVote = function(r) {
		vvDebugg('RATE '+p.settings.type+' '+p.settings.idx+' : '+r);
		vvDosubmit = true;	
		var fUrl = "";
		
		$(p).fadeTo(100,0.5)
		p.settings.sending = true;
		
		if(p.settings.vUrl){
			fUrl = p.settings.url+'&vUrl='+encodeURIComponent(document.URL);
		}else{
			fUrl = p.settings.url;
		}
		vvDebugg(fUrl);
		$.ajax({
			url:fUrl,			
			data:{idx:p.settings.idx,type:p.settings.type,rate:r},
			type:'POST',
			dataType:'JSON',
			beforeSend: function(){
				vvDebugg(p.settings);
				vvLoadShow();
			},
			complete: function(data){					 		
				vvDosubmit = false;
				vvLoadHide();
				p.settings.sending = false;
				p.settings.start = true;
			},
			success: function(data){				
				if(p.settings.callback != null){
					p.settings.callback(data);
				}else{
					p.callback(data);
				}
			}
		});
	};
	p.init();	
};





/*(function (window) {
	/*global CustomEvent  
	'use strict';
	var devtools = window.devtools = { open: false };
	var threshold = 160;
	var emitEvent = function (state) {
		window.dispatchEvent(new CustomEvent('devtoolschange', {
			detail: {
				open: state
			}
		}));
	};

	setInterval(function () {
		if (window.outerWidth - window.innerWidth > threshold ||
			window.outerHeight - window.innerHeight > threshold) {
			if (!devtools.open) {
				emitEvent(true);
			}
			devtools.open = true;
		} else {
			if (devtools.open) {
				emitEvent(false);
			}
			devtools.open = false;
		}
	}, 500);
})(window);
*/

var vvConsoleOpen = false;
window.onload = function(){
	if ('undefined' == typeof window.jQuery) {		 
		alert('jQuery not loaded');
	} else {
		
	}
	var vvString = '<!-- \n' +
    '                                          o8o      .               \n' +
    '                                          `"\'    .o8               \n' +
    ' .ooooo.  oo.ooooo.   .oooo.    .ooooo.  oooo  .o888oo oooo    ooo \n' +
    'd88\' `88b  888\' `88b `P  )88b  d88\' `"Y8 `888    888    `88.  .8\'  \n' +
    '888   888  888   888  .oP"888  888        888    888     `88..8\'   \n'+
	'888   888  888   888 d8(  888  888   .o8  888    888 .    `888\'    \n'+
	'`Y8bod8P\'  888bod8P\' `Y888""8o `Y8bod8P\' o888o   "888"     .8\'     \n'+
	'           888                                         .o..P\'      \n'+
	'          o888o                                        `Y8P\' \n'+
	'\n'+
	'-->';
	
	setTimeout(function(){
		
		$('head').prepend(vvString);
		
	},1000);
	if (window.console) {
		// get notified when it's opened/closed
		addEvent('devtoolschange',window, function (e) {
			if( e.detail.open ){
				console.log('Så du gillar att titta under huven?');
			}else{
				 
			}		
		});
	}
	
};


function addEvent(evnt, elem, func) {
   if (elem.addEventListener)  // W3C DOM
      elem.addEventListener(evnt,func,false);
   else if (elem.attachEvent) { // IE DOM
      elem.attachEvent("on"+evnt, func);
   }
   else { // No much to do
      elem[evnt] = func;
   }
}


function vvDebugg(message){
	 var caller_t = arguments.callee.caller.toString().split('function');
	 var caller_s = caller_t[1].split('{');		 
	 if(caller_s[0] != " ()"){
		caller = ''+caller_s[0]+': ';
	 }else {
		caller = null;
	 }
	 level = false;
	 if(debugg){
		if (window.console) {
			if (!level || level === 'info') {
				 if(caller == null){
					window.console.log(message);
				 }else{
					 window.console.log(caller,message);
				 }
			}
			else
			{
				if (window.console[level]) {
					
					window.console[level](message);
				}
				else {
					window.console.log('<' + level + '> ' + message);
				}
			}
		}
	}
}


/**
 * vvColums
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Gjord för sac
 *
 
$.fn.vvColums = function(gridDiv) {
	grid = {total:$(this).length,cols:0};
	$(gridDiv).find('.col').each(function(){
		if($(this).css('display') != 'none'){
			grid.cols++;
		}
	});
	grid.e = Math.round(grid.total/grid.cols)+1;
	grid.i = 1;
	grid.s = 1;
	 
	this.each(function(){
		gridDiv.find("#col"+grid.i).append($(this));		 
		grid.s++;
		if(grid.s>=grid.e){
			grid.s = 1;
			grid.i++;
			if(grid.i>grid.cols){
				grid.i = 3;
			}
		}
	});
}
*/



$.fn.vvColums = function(gridDiv) {
	grid = {total:$(this).length,cols:0,n:0,float:0,fraction:0};
	$(gridDiv).find('.col').each(function(){
		if($(this).css('display') != 'none'){
			grid.cols++;
		}
	});
	
	grid.e =Math.floor(grid.total/grid.cols);
	grid.n = grid.total/grid.cols;
	 
	grid.float = (grid.n-Math.floor(grid.n));     
	grid.fraction = Math.floor(grid.float*10);
	grid.diff = 0;		 
	
	if(grid.fraction>0){
		grid.diff = 1;
		// add 1 item to first row
		 
	}
	if(grid.fraction>3){
		grid.diff = 2;
		// add 1 item to first row
		 
	}
	
	
	grid.i = 1;
	grid.s = 1;
	 
	this.each(function(){
		 
		gridDiv.find("#col"+grid.i).append($(this));
		if(grid.diff>0){
			d = 1;
		}else{
			d = 0;
		}
		
		if(grid.s >= grid.e+d){
			grid.s = 0;					
			grid.i++;
			grid.diff--;	
			if(grid.diff<=0){
				grid.diff = 0;
			}
			if(grid.i>grid.cols){
				
			}
		}
		grid.s++;
		
		
	});
}

function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script');
  fileref.setAttribute("type","text/javascript");
  fileref.setAttribute("src", filename);
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", filename);
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref);
}



function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}


function truncate(string,l){
	
   if (string.length > l)
      return string.substring(0,(l-3))+'...';
   else
      return string;
};

function vvTemplate(html,array){
	for(var o in array){
			 
		var re = new RegExp("{"+o+"}", 'g');
		html = html.replace(re, array[o]);
	}
	return html
}


/**
 * vvGetData
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Detta är gjort för marcovukota
 *
 */
 
$.fn.vvGetData = function(options){	
		
	var defaults = {
		parent	:	null,
		url		: 	"/?init=none&vvAjaxRequest=true",
		dataType: 	"html", // xml, json, script, or html 
		type	: 	"GET",// GET
		callback: 	null,
		parms	: {}
	}							
	var plugin = this;							
	plugin.settings = {};							
	plugin.init = function() {									 	
		plugin.settings = $.extend({}, defaults, options);	
		plugin.vvAjax();
		 
	}
	plugin.callback = function(data){
		
		
		$(plugin).html(data);
		$(document).trigger('vvInit');
	};
	plugin.vvAjax = function() {
		vvDebugg(plugin.settings.url);	 
		vvDosubmit = true;	
		jQuery.support.cors = true;
		$.ajax({
			url:plugin.settings.url+'&vvAjaxRequest=true',			
			data:plugin.settings.parms,
			type:plugin.settings.type,
			dataType:plugin.settings.dataType,
			cache: false,
			crossDomain: true,
			beforeSend: function(){
				
				vvDebugg(plugin.settings);
				vvLoadShow();
			},
			complete: function(data){					 		
				vvDosubmit = false;
				vvLoadHide();
				vvDebugg('vvGetData');
				if(IE){
					 
				}
			},
			
			success: function(data){
				 
				
				if(plugin.settings.callback != null){
					plugin.settings.callback(data);
				}else{
					plugin.callback(data);
				}
			}
		});
	};
	plugin.init();
	
};



/**
 * vvFullScreenGallery
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Detta är gjort för marcovukota
 *
 */
var current_gallery = null;
var current_img = null;
var current_img_index = 0;

$.fn.vvFullScreenGallery = function(){	
	this.each(function(index, obj){	
		$(obj).unbind('click').bind('click',function(e) {
			
			/* first time setup */
			current_img_index = 0;
			current_gallery = $(this);			
			current_gallery.t = $(this).attr('gallery').split('|').length;			
			
			current_gallery.unbind('next').bind('next',function(){
				vvDebugg('next');
				current_img_index++;
				if(current_img_index>=current_gallery.t){
					current_img_index = 0;
				}
				current_gallery.trigger('gallery');
			});
			current_gallery.unbind('prev').bind('prev',function(){
				vvDebugg('prev');
				current_img_index--;
				if(current_img_index<0){
					current_img_index = current_gallery.t-1;
				}
				current_gallery.trigger('gallery');
			});
					
			current_gallery.bind('gallery',function(){
				
				/* trigger gallery for gallery action */
				
				
				var g = current_gallery.attr('gallery').split('|');	
				var x = g[current_img_index].split(':');
				
				current_img = $("#f_"+x[0]);			
				if(current_img.get(0) == undefined){
					vvDebugg('image not loaded');
					return;
				}
				if($("#full_image_view").get(0) == undefined){
					$(body).append('<div id="full_image_view" class="fullscreen"><div class="exit-button"></div><div class="arrow" id="arrow-left"></div><div class="arrow" id="arrow-right"></div><div class="ii_img" style="width:100%;height:100%;"></div><div class="ii_title"></div></div>');				
					setTimeout(function(){
						
						$("#full_image_view .ii_img").anystretch(current_img.attr('src'), {speed: 150});
						
					},500);
					$("#full_image_view").on( "swipeleft", swipeleftHandler );
					$("#full_image_view").on( "swiperight", swiperightHandler );
				}else{
					$("#full_image_view .ii_title").html(current_img.attr('title'));
					$("#full_image_view .ii_img").anystretch(current_img.attr('src'), {speed: 150});
				}	
				$("#full_image_view .ii_title").hide().html('');
				setTimeout(function(){
					if(current_img.attr('title') != ""){
					$("#full_image_view .ii_title").html(current_img.attr('title')).delay(400).fadeTo(500,1);		
					}
				},800);
				$("#full_image_view").show();
				$(".exit-button").unbind('click').bind('click',function(){
					$("#full_image_view").hide();
				});
				$("#arrow-right").unbind('click').bind('click',function(){
					 current_gallery.trigger('next');
				});
				$("#arrow-left").unbind('click').bind('click',function(){
					 current_gallery.trigger('prev');
				});	
			});
			
			current_gallery.trigger('gallery');
		});
	});
	function swipeleftHandler(){
		current_img.next().trigger('click');
	}
	function swiperightHandler(){
		current_img.prev().trigger('click');
	}	
}



/**
 * vvShorten
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Detta är gjort för telia was
 *
 */
$.fn.vvShorten = function(){
	this.each(function(index, obj){	
		if($(obj).attr('stitle') == undefined){			 
			title = $(obj).text();
			$(obj).attr('stitle',title);
		}else{
			title = $(obj).attr('stitle');
		}
			
		leng = ($(window).width()/11);
		 
		if((title.length*12)>$(this).parent().width()){
			if($(obj).attr('rtitle') != undefined){	
				$(this).html($(obj).attr('rtitle')); 		 
			}else{
				if(title.length>leng){	 
					shortText = jQuery.trim(title).substring(0, leng).split(" ").slice(0, -1).join(" ") + "...";
					$(this).html(shortText); 
				}
			}
		}else{
			$(this).html(title); 
		}
		
	});
	
}


$.fn.preload = function() {
	this.each(function(){
		$('<img/>')[0].src = this;
	});
}
/**
 * vvSelector
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Detta är gjort för noexcuses
 *
 */
$.fn.vvSelector = function() {
	$(this).each(function(index, element) {
        $(this).unbind('change').bind('change',function(e){	 
			$(this).parent().find('span').html($(this).find(':selected').text());	
			$(this).trigger('vvSelectChange');		 
		});	
		 
		 if($(this).find(':selected').text() == ""){
		 	val = $(this).parent().find('span').attr('placeholder');
		 }else{
		 	val = $(this).find(':selected').text();
		 }
		$(this).parent().find('span').html(val);
    }); 
	
	return true;
}
/**
 * vvScrollFade
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Detta är gjort för mood
 *
 */
$.fn.vvScrollFade = function(s) {
	
	if(this.attr('stop') == undefined){
		this.attr('stop',this.offset().top);
	}
	var st = parseInt(this.attr('stop'))+this.height();
	var s = $(window).scrollTop();
	var e = $(".white-page").offset().top;
	var t = this.offset().top+this.height();
	var p = e-t;
	var a = e-st;
	var x = p/a;
	
	this.css({opacity:x});
}


/**
 * vvAbsolutCenter
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att centerar en din efter w
 */
$.fn.vvAbsolutCenter = function(w) {
	
	this.each(function(){
		 
		if($(this).outerWidth()>w){
			var v = Math.max(0,($(this).outerWidth()-w)/2);
		}else{
			var v = 0;
		}
		 
		$(this).css({left:-v});
	});
}
/**
 * vvAbsolutCenter
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att centerar en din efter w
 */
$.fn.vvFixedAtPass = function(s) {
	
	this.each(function(){
		
		if($("."+$(this).attr('id')+'-clone').get(0) == undefined){
			clone = $('<div></div>');
			clone.addClass($(this).attr('id')+'-clone'); 
			clone.addClass($(this).attr('mobile_class')); 
			clone.css({padding:$(this).css('padding')});
			$(this).css('position','absolute');
			$(this).after(clone);
		}else{
			clone = $("."+$(this).attr('id')+'-clone');
		}
		
		clone.css({height:$(this).height()});
		 
		
		if(s >= clone.offset().top){	
			/*
			
			if($(this).attr('lastPos') == undefined){
			$(this).attr('lastPos',$(this).css('position'));
			$(this).attr('lastPosY',$(this).offset().top);
		}
			if($("."+$(this).attr('id')+'-clone').get(0) == undefined){
				clone = $(this).clone();
				clone.addClass($(this).attr('id')+'-clone'); 
				clone.removeClass('fixed-at-pass');
				clone.css({position:'fixed',top:0,zIndex:99999999999999999999});
				$(body).prepend(clone);
				 
			}
			
			
			*/			
			 
			$(this).css({position:'fixed',top:0});
		}else{		
			//$("."+$(this).attr('id')+'-clone').hide();
			 
			$(this).css({position:'absolute',top:'auto'});
		}
	});
}


/**
 * vvCenter
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att centerar en din efter w
 */
$.fn.vvCenter = function(w) {
	
	this.each(function(){
		 
		if($(this).css('display') == 'none'){
			return true;
		}
		
		var v = Math.max(0,(w-$(this).outerWidth())/2);
		 
		$(this).css({left:v});
	});
}
/**
 * vvMiddle
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att centerar horisontelt en din efter h och scroll
 */
$.fn.vvMiddle = function(h,s,ani) {
	if(ani == undefined){
		ani = false;
	}
	this.each(function(){	
		
		if($(this).css('display') == 'none'){
			return true;
		}
		
		var m = Math.max(0,((h-$(this).outerHeight())/2)+s);
	 
		
		if(ani){
			$(this).stop().animate({top:m},100);
		}else{
			$(this).offset({top:m})
		}
		 
	});
}
/**
 * vvFullWith
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att gör div lika som w
 */
$.fn.vvFullWith = function(w){
	if($(this).css('display') == 'none'){
		return true;
	}
	pad = parseInt($(this).css('paddingLeft'))+parseInt($(this).css('paddingRight'));
	margin = parseInt($(this).css('marginLeft'))+parseInt($(this).css('marginRight'));
	border = parseInt($(this).css('border'));
	v = w-pad-margin-border;	 
	$(this).css('width',v);
}

/**
 * vvFullWith
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att gör div lika som h
 */
$.fn.vvFullHeight = function(h){
	if($(this).css('display') == 'none'){
		return true;
	}
	pad = parseInt($(this).css('paddingTop'))+parseInt($(this).css('paddingBottom'));
	border = parseInt($(this).css('border'));
	v = h;	 
	$(this).css('height',h);
	 
}


/**
 * vvDivResponsive 
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att göra video scale prop
 */
$.fn.vvDivResponsive = function(w,h){	
	var width = w;    // 
	var height = h;  //
	 
	if(width == undefined){
		return;
	}
	if(height == undefined){
		return;
	}
	//original height / original width x new width = new height
	dHeight = height/width * $(this).width();
	
	$(this).css('height',dHeight);
}
/**
 * vvDivStretch 
 *
 * @ Denna funktion ligger i vvjQuery.js
 * @ 
 */
$.fn.vvDivStretch = function(w,h,sw,sh){
	 
	var maxWidth = sw; //  
	var maxHeight = sh;    // 
	var ratio = 0;  // Used for aspect ratio
	var width = w;    // 
	var height = h;  //  
	 vvDebugg(w+', '+h+', '+sw+', '+sh);
	// Check if the current width is larger than the max
	if(width < maxWidth){
		
		/* get ratio for scaling image*/
		ratio = maxWidth/width;
		vvDebugg(ratio);
		$(this).css("width", maxWidth);// Set new width
		$(this).css("height", height * ratio);  // Scale height based on ratio
		
		height = height * ratio;// Reset height to match scaled image
		width = width * ratio;// Reset width to match scaled image
		 
		 
	}
	 if(height < maxHeight){ 	// Check if current height is larger than max
		/* get ratio for scaling image*/
		ratio = maxHeight/height;
		vvDebugg('H:'+ratio);
		$(this).css("height", maxHeight);// Set new height
		$(this).css("width", width * ratio);// Scale width based on ratio
		
		width = width*ratio;// Reset width to match scaled image
	 
	}	 
}
/**
 * vvVideoStretch 
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att gör video Stretch
 */
$.fn.vvVideoStretch = function(w,h){
	 
	var maxWidth = w; // Max width for the image
	var maxHeight = h;    // Max height for the image
	var ratio = 0;  // Used for aspect ratio
	var width = $(this).attr('mwidht');    // Current image width
	var height = $(this).attr('mheight');  // Current image height
	 
	// Check if the current width is larger than the max
	if(width < maxWidth){
		
		/* get ratio for scaling image*/
		ratio = maxWidth/width;
		vvDebugg(ratio);
		$(this).css("width", maxWidth);// Set new width
		$(this).css("height", height * ratio);  // Scale height based on ratio
		
		height = height * ratio;// Reset height to match scaled image
		width = width * ratio;// Reset width to match scaled image
		 
		 
	}
	 if(height < maxHeight){ 	// Check if current height is larger than max
		/* get ratio for scaling image*/
		ratio = maxHeight/height;
		vvDebugg('H:'+ratio);
		$(this).css("height", maxHeight);// Set new height
		$(this).css("width", width * ratio);// Scale width based on ratio
		
		width = width*ratio;// Reset width to match scaled image
	 
	}	 
}


/**
 * vvVideoResponsive 
 *
 * @Denna funktion ligger i vvjQuery.js
 * @För att göra video scale prop
 */
$.fn.vvVideoResponsive = function(){	
	var width = $(this).attr('mwidht');    // Current image width
	var height = $(this).attr('mheight');  // Current image height
	 
	if(width == undefined){
		return;
	}
	if(height == undefined){
		return;
	}
	//original height / original width x new width = new height
	videoHeight = height/width * $(this).width();
	
	$(this).css('height',videoHeight);
}



/**
 * vvTabs
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Gjort för picnic
 */

var vvTabsObj = new Array();
var vvTabsBind = false;
var vvTabsPageNit = 5;
var vvTabsPageNr = 0;
var vvAjaxPagenit = false;
$.fn.vvTabs = function(obj,objs) {
	vvDebugg('Do vvtabs');
	vvTabsObj = new Array();
	this.each(function(){
		f = $(this);
		
		f.tabsHolder = $(this).find('.tabs');
		f.itemsHolder = $(this).find('.items');
		f.tabs = new Array();
		if(objs == undefined){
			f.objs = new Array();
		}else{
			f.objs = new Array();
		}
		f.objClass = obj.itemsClass;
		$(obj.itemsClass).each(function(index, element) {
			db = $(element).attr('db');
			cat = $(element).attr('db');
			
			if( $.inArray(db,f.tabs) == -1){
				 
				f.objs[db] = {c:0,id:db};
				f.tabs.push(db);
				 
			 	if($('#tabs-'+db).get(0) == undefined){
					f.tabsHolder.append('<div id="tabs-'+db+'" class="tabItem" db="'+db+'">'+cat+' <span id="count-'+db+'">(0)</span></div>');
				}
				if($('#tab-'+db).get(0) == undefined){
					f.itemsHolder.append('<div class="tab-item-holder" id="tab-'+db+'"></div>');
				}
				
			}
			clone = $(element).clone();
			if(vvAjaxPagenit){
				if(f.objs[db].c>vvTabsPageNit){
					clone.hide();
				}
			}
			
            $('#tab-'+db).append(clone);
			f.objs[db].c++;
			
			$(element).remove();
        });
		
		
		if(f.tabs.length>0){
		
			$(f.tabs).each(function(index, element) {
				id = f.objs[f.tabs[index]].id;
				c  = f.objs[f.tabs[index]].c;
				$("#count-"+id).html('('+c+')');
				
			});
			
			$('#tab-'+f.objs[f.tabs[0]].id).addClass('aktiv');
			$('#tabs-'+f.objs[f.tabs[0]].id).addClass('aktiv');
			
			
			$(".tabItem").unbind('click').bind('click',function(){
				$('.pagenit-hold').hide();
				$(".aktiv").removeClass('aktiv');
				$(this).addClass('aktiv');
				$("#tab-"+$(this).attr('db')).addClass('aktiv');
				$('.pagenit-'+$(this).attr('db')).show();
				 
			})
			
			vvTabsObj.push(f)
		}
		
		
		if(!vvTabsBind){
			vvDebugg('Tabs init function bind');
			vvTabsBind = true;
			$(document).bind('vvInit',function(){
				vvDebugg('Call tabs init ');
				
				
				
				for(var i = 0;i<vvTabsObj.length;i++){
					f = vvTabsObj[i];
					$(f.tabs).each(function(index, element) {
						id = f.objs[f.tabs[index]].id;
						
						c  = 0;
						$("#tab-"+id).find(f.objClass).each(function(index, element) {
							c++;
						});
						$("#count-"+id).html('('+c+')');
						
						if(vvAjaxPagenit){
						
							pages = c/vvTabsPageNit;
							pagnit = f.find('.pagenit-'+id);
							pagnit.html('');
							for(var i = 0;i<pages;i++){
								 
								if(pagnit.get(0) == undefined){
									f.append('<div class="pagenit-hold pagenit-'+id+'"></div>');
									pagnit = f.find('.pagenit-'+id);
								}else{
									
								}
								pagnit.append('<a href="pagenit-'+i+'" db="'+id+'" idx="'+i+'" objClass="'+f.objClass+'" class="pagenit db-'+id+' noAjax">'+(i+1)+'</a>');
								
							}
						}
						
					});
				}
				
				id = $('.tabs').find('.aktiv').attr('db');
				tab = $('#tab-'+id);
				objClass = '.item';
				$('.pagenit-hold').hide();
				$('.pagenit-'+id).show();
				if(vvAjaxPagenit){
					vvPageNit(tab,id,objClass);
				
					$(".pagenit").unbind('click').bind('click',function(e){
						$(".pagenit-hold").find('.aktiv').removeClass('aktiv');
						$(this).addClass('aktiv');
						
						e.preventDefault();
						
						vvTabsPageNr = $(this).attr('idx');
						vvPageNit(tab,id,objClass);
						
						
					});
				}
				
				
			});
		}
		$(document).trigger('vvInit');
	});
	
}

function vvPageNit(tab,id,objClass){
	i = 0;
	pageStart = (vvTabsPageNit*vvTabsPageNr);
	pageEnd = ((vvTabsPageNit*vvTabsPageNr)+vvTabsPageNit);
	
	vvDebugg(pageStart+' || '+pageEnd+'- - '+tab.attr('id')+' - '+objClass+' -< '+tab.find(objClass).length);
	
	tab.find(objClass).show();
	tab.find(objClass).each(function(){
		if(i >= pageStart && i <= pageEnd){
			$(this).show();
		}else{
			$(this).hide();
		}
		i++;
	});

}


function vvIsNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
/**
 * vvSerialize
 *
 * @Victors egna serialize function för vvajax
 */

$.fn.vvSerialize = function() {
	var radio = new Array();
	var parms = {};
	var i = 0;
	var multiInputs = new Array();
	this.each(function(){		
		obj = $(this);		 
		if(obj.attr('req') != undefined || obj.hasClass('req')){			
			if (obj.attr("type") == "checkbox" && !obj.is(':checked')) {
				parms[obj.attr("name")] = '_alert'+$(this).attr('req_text');
			}else{
				if(obj.val() == "" || obj.val() == obj.attr('placeholder')){					
					parms[obj.attr("name")] = '_empty';
					obj.parent().css({border:'1px solid red'});					
					vvMsgBox($(this).attr('req_text'),null,4000);					
				}else{
					obj.parent().css({border:''});
				}
			}
		}
		
		if(obj.hasClass('req_mail')){
			if(!validEmail(obj.val()) || obj.val() == ""){
				parms[obj.attr("name")] = '_empty';
				obj.parent().css({border:'1px solid red'});
				vvMsgBox(allText.validEmail,null,4000);
			}else{
				obj.parent().css({border:''});
			}
		}
		if(obj.hasClass('req_number')){
			vvDebugg('NR: '+vvIsNumeric(obj.val()));
			if(!vvIsNumeric(obj.val()) || obj.val() == ""){
				parms[obj.attr("name")] = '_empty';
				obj.parent().css({border:'1px solid red'});
				vvMsgBox(allText.validNumber,null,4000);
			}else{
				obj.parent().css({border:''});
			}
		}
		if(obj.attr('req_length') != undefined){
			 
			if(obj.val() == "" || obj.val().length < parseInt(obj.attr('req_length'))){
				parms[obj.attr("name")] = '_empty';
				obj.parent().css({border:'1px solid red'});
				vvMsgBox(allText.reqLength,null,4000);
			}else{
				obj.parent().css({border:''});
			}
		}
		if(parms[obj.attr("name")] == undefined){			
			if(parms[obj.attr("name")] != '_empty'){
				if (obj.hasClass("summernote")) {
					parms[obj.attr("name")] = obj.code();
					vvDebugg(parms[obj.attr("name")]);
				}else if (obj.attr("type") == "radio") {
					if(obj.is(':checked')){
						parms[obj.attr("name")] = obj.val();				
					}
					if(obj.hasClass('req') && obj.parent().parent().find("input:checked").length == 0){
						obj.parent().parent().css({border:'1px solid red'});
						vvMsgBox($(this).attr('req_text'),null,4000);
						parms[obj.attr("name")] = '_empty';
					}else{
						obj.parent().parent().css({border:''});
					}
				}else if (obj.attr("type") == "checkbox") {
					if(obj.hasClass('checkboxPlus')){						 
						if(obj.is(':checked')){
							if(parms[obj.attr("name")] == "" || parms[obj.attr("name")] == undefined){
								parms[obj.attr("name")] = obj.val();
							}else{
								parms[obj.attr("name")] += ","+obj.val();
							}
						}
					}else{
						if(obj.is(':checked')){
							parms[obj.attr("name")] = 1;
						}else{
							parms[obj.attr("name")] = 0;
						}
					}
				  
				}else{
					if(obj.attr("name") != undefined){
						if(obj.attr("name").match(/\[(.*?)\]/)){
							n = obj.attr("name");
							regex = /(.*?)\[\]/gi;
							m = regex.exec(n);
							
							if(m != null){
								if(multiInputs[m[1]] == undefined){
									multiInputs[m[1]] = 0;
								}					
								multiInputs[m[1]]++;
								parms[m[1]+'['+multiInputs[m[1]]+']']  = obj.val();
							}else{
								parms[n] = obj.val();								
							}
						}else{
							parms[obj.attr("name")] = obj.val();
						}
					}
					
				}
			}
		}else{
			vvDebugg('Empty -> '+obj.attr("name"));
		}
		i++;
	});	 
	for(var k in parms){		 
		if(parms[k] != null && (typeof parms[k] == "string")){
			parms[k] = parms[k].replace('+','&#43;');			
			parms[k] = encodeURI(parms[k]);
		}
	}
	
	if(parms.length> 0){
		vvDebugg(parms);
	}
	
	parms.length = i;
	return parms;
}


/**
 * vvSerializeDiv by indice
 *
 * @vvSerializeDiv
 */
$.fn.vvSerializeDiv = function() {
	var parms = {};
	var i = 0;
	this.each(function(){
		obj = $(this);
		parms[obj.attr("id")] = i;
		obj.attr("indice",i);
		i++;
	});	
	parms.length = i;
	return parms;
}


/**
 * vvSortList by indice
 *
 * @Sorterat div lista
 */
$.fn.vvSortList = function(c) {
	 
	$(this).html($(c).sort(function(a, b) {
		return $(a).attr('indice') - $(b).attr('indice');
	}));
}



/**
 * vvAttackTrim
 *
 * @Rensar sring from script
 */
function vvAttackTrim(v){
	v = v.replace('script','');
	v = v.replace('<!','');
	v = v.replace('script','');
	return v;
}







/**
 * vvMobileMenu
 *
 * @Denna funktion ligger i vvjQuery.js
 * @$(menuid).vvMobileMenu(args);
 *
 * @args {
 * direction : left/right
 * objects : object typ
 * btn : Knapp för aktivering
 * contentWrap : vilken div sidan ligger i 
 * }
 
var vvmenu = new Array();
$.fn.vvMobileMenu = function(args) {
	vvDebugg('vvMobileMenu 1');
	id = $(this).attr('id');
	
	if(vvmenu[id] == undefined){
		return;
	}
	if(args == 'close'){
		vvDebugg('close menu '+id);
		if(vvmenu[id].open){	
			vvmenu[id].fnClose();			
		}
		return;	
	}
	 vvDebugg('vvMobileMenu DONE');
	if(args == 'open'){
		vvDebugg('Open menu '+id);
		return;	
	}
	if(args == 'toggle'){
		vvDebugg('toggle menu '+id);
		vvDebugg(vvmenu[id]);
		var w = $(window).width();
		
		if(vvmenu[id].open){			
			move('#'+vvmenu[id].contentWrap).set(vvmenu[id].direction,0).end();
			move('#'+vvmenu[id].mobileWrap).set('width',0).end();
			 
			vvmenu[id].open = false;			
		}else{	
			cw = w-100;
			move('#'+vvmenu[id].contentWrap).set(vvmenu[id].direction,cw).end();
			move('#'+vvmenu[id].mobileWrap).set('width',cw).end();
			
			vvmenu[id].open = true;
		}		
		return;	
	}	
	 vvDebugg('vvMobileMenu DONE');
	$(document).bind('vvWindowBigDiff',function(){
		for(e in vvmenu){
			vvmenu[e].fnAction(e);
		}
	});
	
	vvmenu[id] = {};
	 
	vvmenu[id].direction = 'left';
	vvmenu[id].objects = 'div';
	vvmenu[id].btn = 'menu-open';
	vvmenu[id].width = 300;
	vvmenu[id].open = false;
	vvmenu[id].aktiv = false;
	vvmenu[id].contentWrap = 'wrapper'
	vvmenu[id].obj = this;
	vvmenu[id].mobileWrap = 'mobile-wrapper-'+id;
	vvmenu[id].MenuClass = 'mobile-menu-wrap';
	
	vvmenu[id].fnClose = function(){
			move('#'+vvmenu[id].contentWrap).set(vvmenu[id].direction,0).end();
			move('#'+vvmenu[id].mobileWrap).set('width',0).end();
			
			vvDebugg('');
			
			vvmenu[id].open = false;
	}
	vvmenu[id].fnShow = function(){
		 vvmenu[id].obj.hide();
		 vvmenu[id].btnC.show();
	}
	vvmenu[id].fnHide = function(){
		 vvmenu[id].obj.show();
	}
	 vvDebugg('vvMobileMenu DONE');
	vvmenu[id].fnAction = function(e){			
		  if($(body).hasClass('mobile')){			 
			  vvmenu[e].fnClose();
			  vvmenu[e].fnShow();
			  vvmenu[e].btnC.show();
			  $("#"+vvmenu[id].contentWrap).addClass('vvMobile-wrap');
		  }else{
			  $("#"+vvmenu[id].contentWrap).removeClass('vvMobile-wrap');
			  vvmenu[e].fnClose();
			  vvmenu[e].fnHide();
			 vvmenu[e].btnC.hide();
		  }
			
	}
	
	if($("#vvmenucss").get(0) == undefined){
		$('head').append('<link rel="stylesheet" id="vvmenucss" href="/c/vvmenu.css" type="text/css" />');	 
	}	
	// Läser in alla args
	for(a in args){
		vvmenu[id][a] = args[a];
	}	
	contentWrap = $("#"+vvmenu[id].contentWrap);
	mobileWrap = $("#"+vvmenu[id].mobileWrap);
	
	if($("#"+vvmenu[id].btn).get(0) == undefined){
		$(body).prepend('<div id="'+vvmenu[id].btn+'" class="mobile-menu-btn"></div>');
	}
	vvmenu[id].btnC = $("#"+vvmenu[id].btn);
	vvmenu[id].btnC.unbind('click').bind('click',function(e) {
		 
		e.preventDefault();	
		$(vvmenu[id].obj).vvMobileMenu('toggle');
	});
	
	if(mobileWrap.get(0) == undefined){
		$(body).prepend('<div id="'+vvmenu[id].mobileWrap+'">Mobile menu</div>');
		mobileWrap = $("#"+vvmenu[id].mobileWrap);
		mobileWrap.addClass('vvMobileMenu');
		if(vvmenu[id].direction == 'left'){
			mobileWrap.addClass('vvMM-left');
		}else{
			mobileWrap.addClass('vvMM-right');
		}
		
		mobileWrap.html($(this).clone().html());
		
		mobileWrap.find(vvmenu[id].objects).each(function(){
			$(this).attr('id',$(this).attr('id')+'-mobile');
			$(this).click(function(){
				 
			});
		});
		
		
	}
	$(this).addClass(vvmenu[id].MenuClass);
	if($('.mobile-menu-items ').find(vvmenu[id].objects).length > 0){
		mobileWrap.append('<hr>');
		$('.mobile-menu-items ').find(vvmenu[id].objects).each(function(){		
			mobileWrap.append($(this).clone());
		});
	}
	
	//setTimeout(function(){ vvmenu[id].fnAction(id); },100);
	 vvDebugg('vvMobileMenu DONE');
	$(document).trigger('vvInit');

};
*/


(function($) {
	
	
/**
 * vvMobileMenu
 * v2
 * @Denna funktion ligger i vvjQuery.js
 * @$(menuid).vvMobileMenu(args);
 *
 * @args {
 * direction : left/right
 * objects : object typ
 * btn : Knapp för aktivering
 * contentWrap : vilken div sidan ligger i 
 * }
 */
$.vvMobileMenu = function(el,op) {	
    
	
	var defaults = {
		direction	: 'left',
		objects		: 'a',
		btn			: '#mobile-menu-open',
		open		: false,
		aktiv		: false,
		contentWrap	: '#wrapper',
		mobileWrap	: '#mobile-wrapper',
		id			: '',
		MenuClass	: '',
		html		: ''		
	}	
	
 
	var p = this;
	var obj = $(el);
	p.Toggle = function(){
		if(!p.settings.aktiv){
			$(document).trigger('vvMobileMenuChange');
		}
		if(p.settings.aktiv){
			p.Close();
		}else{
			p.Open();
		}
	}
	p.Close = function(){
		 		 
		move(p.settings.contentWrap).set(p.settings.direction,0).end();
		move(p.settings.mobileWrap).set('width',0).end();
		p.settings.aktiv = false;
		setTimeout(function(){
		$(body).removeClass('vvMMO-'+p.settings.btn.replace('#',''));
		},500);
	}
	p.Open = function(){
		 
		var w = $(window).width();					
		var cw = w-100;
			
		move(p.settings.contentWrap).set(p.settings.direction,cw).end();
		move(p.settings.mobileWrap).set('width',cw).end();
		p.settings.aktiv = true;
		
		$(body).addClass('vvMMO-'+p.settings.btn.replace('#',''));
	}
	
	p.init = function() {
		if($(this).data('test')){
			vvDebugg('vvMobileMenu setup is done');		
		  	return false;
		}else{
			 
			p.settings = {};
			$(this).data('test',true);			
			if($("#vvmenucss").get(0) == undefined){
				$('head').append('<link rel="stylesheet" id="vvmenucss" href="/c/vvmenu.css" type="text/css" />');	 
			}			
		}		
	  	p.settings = $.extend({}, defaults, op);			
	  	p.vvSetup();
	}
	
	p.vvSetup = function() {
		var c = null;
		p.settings.html = p.settings.html+obj.clone().html();
		p.settings.id = $(el).attr('id');
		/* Körs när fönstret ändras mer än 40 px */
		$(document).bind('vvWindowBigDiff',function(){			
			p.Close();			
		});
		$(document).bind('vvMobileMenuChange',function(){	
			if(p.settings.aktiv){		
				p.Close();			
			}
		});
		/* Körs när sidan ändras i vvLoadPage */
		$(document).bind('vvPageChange',function(){
			vvDebugg('vvPageChange');			 
			p.Close();			
		});
		
		
		/* Vilken class ska menyn använda sig av */		 
		if(p.settings.direction == 'left'){
			c = 'vvMM-left';
		}else{
			c = 'vvMM-right';
		}		
		
		/* Skapar mobile-menu-btn och bind till click */	
		if($(p.settings.btn).get(0) == undefined){	
			$(body).prepend('<div id="'+p.settings.btn.replace('#','')+'" class="mobile-menu-btn"></div>');
		}
		$(p.settings.btn).unbind('click').bind('click',function(){
			
			vvDebugg(p.settings.id);			
			p.Toggle();
		});	
		
		/* Ska stänga menyn om man klickar utanför. */
		$(body).bind("click", "body", function (e) {
			if ($(p.settings.mobileWrap)[0] != e.target && $(p.settings.btn)[0] != e.target) {
				p.Close();	
			} else if ($(e.target).parents()[0] == $(p.settings.mobileWrap)[0]) {
				p.Close();	
			}
		});
		
		/* Skapar mobile-menu och lägger till html */	
		$(body).prepend('<div id="'+p.settings.mobileWrap.replace('#','')+'" class="'+c+' vvMobileMenu">'+p.settings.html+'</div>');
		$(p.settings.mobileWrap).find('._children').remove();
		 
		/* Ändrar alla a ID till -mobile */
		$(p.settings.mobileWrap).find(p.settings.objects).each(function(){
			
			$(this).attr('id',$(this).attr('id')+'-mobile');
		});
	
		/* kör vv init för att lägga till ajax page load osv */
		$(document).trigger('vvInit');		
	};
	p.init();	
}
$.fn.vvMobileMenu = function(options) {	
							
	return this.each(function() {		 
		if (undefined == $(this).data('vvMobileMenu')) {
			var plugin = new $.vvMobileMenu(this, options);
			$(this).data('vvMobileMenu', plugin);
		}
	});							
}



/**
 * vvSlider2
 * v2
 * @Denna funktion ligger i vvjQuery.js
 * @$(menuid).vvSlider(args);
 *
 * @Det bör finns en _media och _items
 * @Detta är gjort till opacity.se 2013
 * Använing:
 * $("#").vvSlider2();
 * $("#").data('vvSlider2').next();
 * $("#").data('vvSlider2').prev();
 * $("#").data('vvSlider2').update();
 *
 * @args {
 *  
 * }
 */
$.vvSlider2 = function(el,op) {	
    vvDebugg('test');
	
	var defaults = {
		parent : 		null,
		holderName:		'._slidebox', 
		itemName:		'._item', 
		activeClass:	'active', 
		disabledClass:	'disabled', 
		selectedClass:	'aktiv', 
		sliderSize:		680, 
		speed:			1000,
		animate:		true,
		effect:		'slide',
		timer:			null,
		f:    			0,
		i:    			0,
		start:    		0,
		width:			0, 		 
		total:			0,
		onNext: function() {},
		onPrev: function() {}		
	}	
	
 
	var p = this;
	var obj = $(el);
	
	p.Next = function(){		
		
	}
	p.Prev = function(){		 
		
	}
	p.Update = function(){		 
		var i = 0;
		
		obj.css({height:'auto'});
		obj.find(p.settings.itemName).each(function(){
			$(this).attr({'nr':p.settings.i}).addClass('_slide-'+p.settings.i);			 
			if(i == p.settings.start){
				$(this).addClass('aktiv');
				obj.css({'height':$(this).outerHeight()});
			}else{
				
			}			
			//$(this).css({'position':'absolute'});
			i++;
		});
	}	
	p.init = function() {
		if($(this).data('test')){
			vvDebugg('vvSlider2 setup is alredy done');		
		  	return false;
		}else{
			vvDebugg('vvSlider2 setup is done');		
			p.settings = {};
			$(this).data('test',true);
		}		
	  	p.settings = $.extend({}, defaults, op);			
	  	p.vvSetup();
	}
	
	p.vvSetup = function() {
		p.Update();
	
		/* kör vv init för att lägga till ajax page load osv */
		$(document).trigger('vvInit');		
	};
	
	p.init();	
}
$.fn.vvSlider2 = function(options) {							
	return this.each(function() {		 
		if (undefined == $(this).data('vvSlider2')) {
			var plugin = new $.vvSlider2(this, options);
			$(this).data('vvSlider2', plugin);
		}
	});							
}
	
/**
 * vvSlider
 * v 1.0
 * @Denna funktion ligger i vvjQuery.js
 * @Det bör finns en _media och _items
 * @Detta är gjort till opacity.se 2013
 * Använing:
 * $("#premier").vvSlider();
 * $("#premier").data('vvSlider').next();
 * $("#premier").data('vvSlider').prev();
 * $("#premier").data('vvSlider').update();
 */
$.vvSlider = function(element, options) {							
	var defaults = {
		parent : 		null,
		holderName:		'._media', 
		itemName:		'._item', 
		activeClass:	'active', 
		disabledClass:	'disabled', 
		selectedClass:	'aktiv', 
		sliderSize:		680, 
		speed:			1000,
		animate:		true,
		effect:		'slide',
		timer:			null,
		f:    			0,
		i:    			0,
		width:			0, 		 
		total:			0,
		onNext: function() {},
		onPrev: function() {}
	}							
	var plugin = this;							
	plugin.settings = {}							
	var $element = $(element),element = element;							
	plugin.init = function() {									 	
		plugin.settings = $.extend({}, defaults, options);	
		plugin.update();
	}
	 						
	plugin.update = function() {
		 
		
		pad = parseInt($element.css('paddingLeft'))+parseInt($element.css('paddingRight'));		 
		plugin.settings.width = $element.width()+pad;
		
		 
		plugin.settings.i = 0;
		 
		$element.find(plugin.settings.holderName).find(plugin.settings.itemName).each(function(index, obj) {												
			var x = 0;
			
			lf = ((plugin.settings.width*(plugin.settings.i-plugin.settings.f))+x);
			$(obj).removeClass(plugin.settings.activeClass);
			if(lf == 0){
				$(obj).stop().animate({'opacity':1},plugin.settings.speed);
				 			
				$(obj).addClass(plugin.settings.activeClass);
				$(obj).css('zIndex',3000);
			}else{
				$(obj).css('zIndex',1000);
				$(obj).stop().animate({'opacity':0},plugin.settings.speed);
			}
			
			if(plugin.settings.effect == 'slide'){
				if(plugin.settings.animate && $(obj).attr('nr') !== undefined){
					$(obj).stop().animate({left:((plugin.settings.width*(plugin.settings.i-plugin.settings.f))+x)},plugin.settings.speed);
				}else{
					$(obj).css({left:((plugin.settings.width*(plugin.settings.i-plugin.settings.f))+x)});
				}
			}else{
				
			}
			if($(obj).attr('nr') == undefined){
				$(obj).attr('nr',plugin.settings.i);
				$(obj).css({position:'absolute'});
			}
			if($(obj).height()>0){
				$element.find(plugin.settings.holderName).css('height',$(obj).height());
			}
			 
			plugin.settings.i++;
		});
		
		$(document).trigger('vvSliderUpdate');
		
		plugin.settings.total = plugin.settings.i;
	}	
	plugin.next = function() {
		if(plugin.settings.total > (plugin.settings.f+1)){		 
			plugin.settings.f++;
		}else{
			plugin.settings.f = 0;
		}
		plugin.update(); 
	}
	plugin.prev = function() {	
		if(plugin.settings.f>0){
			plugin.settings.f--;
		}else{
			plugin.settings.f = plugin.settings.total;
		}
		
		plugin.update(); 
	}						
	var foo_private_method = function() {
		// code goes here
	}							
	plugin.init();														
}							
$.fn.vvSlider = function(options) {							
	return this.each(function() {
		if (undefined == $(this).data('vvSlider')) {
			var plugin = new $.vvSlider(this, options);
			$(this).data('vvSlider', plugin);
		}
	});							
}	




/**
 * vvGridScroll
 * v 1.0
 * @Denna funktion ligger i vvjQuery.js
 * @Det bör finns _item och _list för att det ska fungera
 * @Detta är gjort till coke vinter 2013
 * Använing:
 * $("#premier").vvGridScroll();
 * $("#premier").data('vvGridScroll').next();
 * $("#premier").data('vvGridScroll').prev();
 * $("#premier").data('vvGridScroll').update();
 */
$.vvGridScroll = function(element, options) {							
	var defaults = {
		parent : 		null,
		holderName:		'._list', 
		itemName:		'._item', 
		activeClass:	'active', 
		disabledClass:	'disabled', 
		selectedClass:	'aktiv', 
		colSize:		280, 
		colSizeSet:		false, 
		animate:		true,
		timer:			null,
		f:    			0,
		i:    			0,
		width:			0, 
		height:			0,
		maxWidth:		0, 
		maxHeight:		0,
		grid:			0,
		total:			0,
		onNext: function() {},
		onPrev: function() {}
	}							
	var plugin = this;							
	plugin.settings = {}							
	var $element = $(element),element = element;							
	plugin.init = function() {					
					 	
		plugin.settings = $.extend({}, defaults, options);	
		if(plugin.settings.timer != null){
			plugin.settings.timerCal = setInterval(function(){
				plugin.next();
			},plugin.settings.timer);
		}
		plugin.update();
	}							
	plugin.update = function() {
		
		pad = parseInt($element.css('paddingLeft'))+parseInt($element.css('paddingRight'));		 
		plugin.settings.width = $element.width()+pad;	
		if(plugin.settings.colSizeSet == 'auto'){			
			$(element).vvDivResponsive(plugin.settings.maxWidth,plugin.settings.maxHeight);			
			plugin.settings.colSize = $(element).width();
			plugin.settings.grid = Math.floor($(element).width()/plugin.settings.colSize);	
					
		}else{
			plugin.settings.grid = Math.floor(plugin.settings.width/plugin.settings.colSize);
		}		
		 
		plugin.settings.gridSize = plugin.settings.width/plugin.settings.grid;		
		plugin.settings.i = 0;
		 
		$element.find(plugin.settings.holderName).find(plugin.settings.itemName).each(function(index, obj) {												
			var x = (plugin.settings.gridSize-plugin.settings.colSize)/2;
			
			lf = ((plugin.settings.gridSize*(plugin.settings.i-plugin.settings.f))+x);
			if(plugin.settings.animate && $(obj).attr('nr') !== undefined){
				$(obj).stop().animate({left:lf});
			}else{
				$(obj).css({left:lf});
			}
			if($(obj).attr('nr') == undefined){
				$(obj).attr('nr',plugin.settings.i);
				$(obj).css({position:'absolute'});
				 
			}
			if(plugin.settings.colSizeSet == 'auto'){
					
					$(obj).css({width:$(element).width()});
				}
			if(lf == 0 && plugin.settings.colSizeSet != 'auto'){
				setTimeout(function(){				 
					$element.css({height:$(obj).height()});
				},200);
			}
			plugin.settings.i++;
			
		});
		plugin.settings.total = plugin.settings.i;
		 
	}	
	plugin.next = function() {
		if(plugin.settings.total > (plugin.settings.f+plugin.settings.grid)){		 
			plugin.settings.f++;
			 
		}else{
			 plugin.settings.f = 0 ;
		}
		plugin.update(); 
	}
	plugin.prev = function() {	
		if(plugin.settings.f>0){
			plugin.settings.f--;
			 
		}else{
			 plugin.settings.f = plugin.settings.total-plugin.settings.grid;
		}
		plugin.update(); 
	}						
	var foo_private_method = function() {
		// code goes here
	}							
	plugin.init();														
}							
$.fn.vvGridScroll = function(options) {							
	return this.each(function() {
		if (undefined == $(this).data('vvGridScroll')) {
			var plugin = new $.vvGridScroll(this, options);
			$(this).data('vvGridScroll', plugin);
		}
	});							
}

})(jQuery);



/**
 * vvResponsiveWidth
 *
 * @Denna funktion ligger i vvjQuery.js
 * @$(class/id).vvResponsiveWidth(window width);
 *
 */
$.fn.vvResponsiveWidth = function(w) {
	this.each(function(){
		pad = 0;
		margin = 0;
		if($(this).attr('owidth') == undefined){
			if($(this).css('width') == 'auto'){
				sw = $(this).width();
			}else{
				sw = $(this).css('width').replace(/px/,'');
			}
			$(this).attr('owidth',sw);
		}
		if($(this).attr('mwidth') == undefined){
			$(this).attr('mwidth',300);
		}
		if($(this).attr('oheight') == undefined){
			$(this).attr('oheight',$(this).height());
		}
				
		 
		pad = parseInt($(this).css('paddingLeft'))+parseInt($(this).css('paddingRight'));
		margin = parseInt($(this).css('marginLeft'))+parseInt($(this).css('marginRight'));
		ow = parseInt($(this).attr('owidth'));	
		
		if((ow+pad) > w){
			if(w > $(this).attr('mwidth')){
				if(!isNaN(parseInt($(this).css('border')))){
					border = parseInt($(this).css('border'));
				}else{
					border = 0;
				}
				left = $(this).offset().left-parseInt($(this).css('marginLeft'));
				if(left<0){
					left = 0;
				}
				if($(body).hasClass('mobile')){
					left = 0;
					formula = (w-left)-pad-(border*2);
				}else{
					formula = w-(border*2)-pad;
				}
				 
				$(this).css('width',formula);
				
			}else{
				
				$(this).css('width',$(this).attr('mwidth')-pad);
			}					
		}else{					
			$(this).css('width',ow);			
		}
		
	});
}




$.fn.vvDragToSelect = function (conf) {
    var c = typeof(conf) == 'object' ? conf : {};

    // Config
    var config = jQuery.extend({
        className:        'jquery-drag-to-select', 
        activeClass:    'active', 
        disabledClass:    'disabled', 
        selectedClass:    'aktiv', 
        scrollTH:        10, 
        percentCovered:    25, 
        selectables:    false, 
        autoScroll:        false, 
        selectOnMove:    true, 
        onShow:            function () {return true;}, 
        onHide:            function () {return true;}, 
        onRefresh:        function () {return true;}
    }, c);

    var realParent    = $(this);
    var parent        = $(this).parent();
	
	if(parent.get(0) == undefined){
		vvDebugg('No parent');
		return false;
	}else{
		vvDebugg('Loading multidrag');
	}	
    var parentOffset    = parent.offset();
    var parentDim        = {
        left:	parentOffset.left, 
        top:	parentOffset.top, 
        width:	parent.width(), 
        height:	parent.height()
    };
    // Current origin of select box
    var selectBoxOrigin = {
        left:	0, 
        top:	0
    };
    // Create select box
    var selectBox = jQuery('<div/>').appendTo(parent).attr('class', config.className).css('position', 'absolute');

    // Shows the select box
    var showSelectBox = function (e) {
        if (parent.is('.' + config.disabledClass)) {
            return;
        }
        selectBoxOrigin.left	= e.pageX - parentDim.left + parent[0].scrollLeft;
        selectBoxOrigin.top		= e.pageY - parentDim.top + parent[0].scrollTop;

        var css = {
            left:        selectBoxOrigin.left + 'px', 
            top:        selectBoxOrigin.top + 'px', 
            width:        '1px', 
            height:        '1px'
        };
        selectBox.addClass(config.activeClass).css(css);
        config.onShow();
    };

    // Refreshes the select box dimensions and possibly position
    var refreshSelectBox = function (e) {
        if (!selectBox.is('.' + config.activeClass) || parent.is('.' + config.disabledClass)) {
            return;
        }
        var left		= e.pageX - parentDim.left + parent[0].scrollLeft;
        var top 		= e.pageY - parentDim.top + parent[0].scrollTop;
        var newLeft		= left;
        var newTop		= top;
        var newWidth	= selectBoxOrigin.left - newLeft;
        var newHeight	= selectBoxOrigin.top - newTop;

        if (left > selectBoxOrigin.left) {
            newLeft        = selectBoxOrigin.left;
            newWidth    = left - selectBoxOrigin.left;
        }

        if (top > selectBoxOrigin.top) {
            newTop        = selectBoxOrigin.top;
            newHeight    = top - selectBoxOrigin.top;
        }

        var css = {
            left:    newLeft + 'px', 
            top:    newTop + 'px', 
            width:    newWidth + 'px', 
            height:    newHeight + 'px'
        };
        selectBox.css(css);

        config.onRefresh();
    };

    // Hides the select box
    var hideSelectBox = function (e) {
        if (!selectBox.is('.' + config.activeClass) || parent.is('.' + config.disabledClass)) {
            return;
        }
        if (config.onHide(selectBox) !== false) {
            selectBox.removeClass(config.activeClass);
        }
    };

    // Scrolls parent if needed
    var scrollPerhaps = function (e) {
        if (!selectBox.is('.' + config.activeClass) || parent.is('.' + config.disabledClass)) {
            return;
        }

        // Scroll down
        if ((e.pageY + config.scrollTH) > (parentDim.top + parentDim.height)) {
            parent[0].scrollTop += config.scrollTH;
        }
        // Scroll up
        if ((e.pageY - config.scrollTH) < parentDim.top) {
            parent[0].scrollTop -= config.scrollTH;
        }
        // Scroll right
        if ((e.pageX + config.scrollTH) > (parentDim.left + parentDim.width)) {
            parent[0].scrollLeft += config.scrollTH;
        }
        // Scroll left
        if ((e.pageX - config.scrollTH) < parentDim.left) {
            parent[0].scrollLeft -= config.scrollTH;
        }
    };

    // Selects all the elements in the select box's range
    var selectElementsInRange = function () {
        if (!selectBox.is('.' + config.activeClass) || parent.is('.' + config.disabledClass)) {
            return;
        }

        var selectables        = realParent.find(config.selectables);
        var selectBoxOffset    = selectBox.offset();
        var selectBoxDim    = {
            left:    selectBoxOffset.left, 
            top:    selectBoxOffset.top, 
            width:    selectBox.width(), 
            height:    selectBox.height()
        };
		
        selectables.each(function (i) {
            var el            = $(this);
            var elOffset    = el.offset();
            var elDim        = {
                left:    elOffset.left, 
                top:    elOffset.top, 
                width:    el.width(), 
                height:    el.height()
            };

            if (percentCovered(selectBoxDim, elDim) > config.percentCovered) {
                el.addClass(config.selectedClass);
				 
            }
            else {
			 
				if(!isCtrlPressed){
                	el.removeClass(config.selectedClass);
				}
            }
        });
		
    };

    // Returns the amount (in %) that dim1 covers dim2
    var percentCovered = function (dim1, dim2) {
        // The whole thing is covering the whole other thing
        if (
            (dim1.left <= dim2.left) && 
            (dim1.top <= dim2.top) && 
            ((dim1.left + dim1.width) >= (dim2.left + dim2.width)) && 
            ((dim1.top + dim1.height) > (dim2.top + dim2.height))
        ) {
            return 100;
        }
        // Only parts may be covered, calculate percentage
        else {
            dim1.right        = dim1.left + dim1.width;
            dim1.bottom        = dim1.top + dim1.height;
            dim2.right        = dim2.left + dim2.width;
            dim2.bottom        = dim2.top + dim2.height;

            var l = Math.max(dim1.left, dim2.left);
            var r = Math.min(dim1.right, dim2.right);
            var t = Math.max(dim1.top, dim2.top);
            var b = Math.min(dim1.bottom, dim2.bottom);

            if (b >= t && r >= l) {
            /*    $('<div/>').appendTo(document.body).css({
                    background:    'red', 
                    position:    'absolute',
                    left:        l + 'px', 
                    top:        t + 'px', 
                    width:        (r - l) + 'px', 
                    height:        (b - t) + 'px', 
                    zIndex:        100
                }); */

                var percent = (((r - l) * (b - t)) / (dim2.width * dim2.height)) * 100;

            //    alert(percent + '% covered')

                return percent;
            }
        }
        // Nothing covered, return 0
        return 0;
    };

    // Do the right stuff then return this
    selectBox
        .mousemove(function (e) {
            refreshSelectBox(e);
            if (config.selectables && config.selectOnMove) {            
                selectElementsInRange();
            }
            if (config.autoScroll) {
                scrollPerhaps(e);
            }
            e.preventDefault();
        })
        .mouseup(function(e) {
            if (config.selectables) {            
                selectElementsInRange();
            }
            hideSelectBox(e);
            e.preventDefault();
        });

    if (jQuery.fn.disableTextSelect) {
        parent.disableTextSelect();
    }
	 
    $(this)
        .mousedown(function (e) {
			 
			 
			if(!isShiftPressed){
				return;
			}
            // Make sure user isn't clicking scrollbar (or disallow clicks far to the right actually)
            if ((e.pageX + 20) > jQuery(document.body).width()) {
                return;
            }
            showSelectBox(e);

            e.preventDefault();
        })
        .mousemove(function (e) {
            refreshSelectBox(e);
            if (config.selectables && config.selectOnMove) {            
                selectElementsInRange();
            }
            if (config.autoScroll) {
                scrollPerhaps(e);
            }
            e.preventDefault();
        })
        .mouseup(function (e) {
            if (config.selectables) {            
                selectElementsInRange();
            }
            hideSelectBox(e);
            e.preventDefault();
        });

    // Be nice
    return this;
};




/**
 * vvWhiteFrame
 *
 * @Denna funktion ligger i vvjQuery.js
 * @Tänkt för moodsthlm
 *
 */
function vvWhiteFrame(w,h){
	padding = 10;
	frameLeft = padding;
	frameRight = padding;
	frameWidth = (w-frameRight)-frameLeft;
	frameTop = 60;
	frameBottom = 10;
	frameHeight = h-frameTop-frameBottom;
	
	frameWhitePage = ((frameWidth-$(".white-page").width())/2)-5;
	
	if(vvClass == 'mobile'){
		frameHeight = frameHeight+45;
		$("#bottomleft").css({width:frameWidth,height:1,left:frameLeft,bottom:'',top:frameHeight+frameTop});
		$("#bottomright").css({width:0,height:1,bottom:frameBottom,right:frameRight});
	}else{
		
		$("#bottomleft").css({width:frameWhitePage,height:1,bottom:frameBottom,left:frameLeft,top:''});
		$("#bottomright").css({width:frameWhitePage,height:1,bottom:frameBottom,right:frameRight});
		
	}
	
	$("#top").css({width:frameWidth,top:frameTop,left:(w-frameWidth)/2});
	$("#left").css({width:1,height:frameHeight,top:frameTop,left:frameLeft});
	$("#right").css({width:1,height:frameHeight,top:frameTop,right:frameRight});
	
}

function htmlspecialchars (string, quote_style, charset, double_encode) {

  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined' || quote_style === null) {
    quote_style = 2;
  }
  string = string.toString();
  if (double_encode !== false) { // Put this first to avoid double-encoding
    string = string.replace(/&/g, '&amp;');
  }
  string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      }
      else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/'/g, '&#039;');
  }
  if (!noquotes) {
    string = string.replace(/"/g, '&quot;');
  }

  return string;
}
function htmlspecialchars_decode (string, quote_style) {
  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined') {
    quote_style = 2;
  }
  string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');
  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
    // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
  }
  if (!noquotes) {
    string = string.replace(/&quot;/g, '"');
  }
  string = string.replace(/&amp;/g, '&');
  return string;
}

$.fn.vvHtml = function(arr) {
	for(d in arr){
		arr[d] = htmlspecialchars_decode(arr[d]);
			 
	}
	return arr;
}


jQuery.fn.insertAt = function(index, element) {
  var lastIndex = this.children().size()
  if (index < 0) {
    index = Math.max(0, lastIndex + 1 + index)
  }
  this.append(element)
  if (index < lastIndex) {
    this.children().eq(index).before(this.children().last())
  }
  return this;
}

/**
 * vvPlaceholder
 *
 * @Denna funktion ligger i vvjQuery.js
 * @vvPlaceholder gjordes för coke vinter
 */
 
$.fn.vvPlaceholder = function(){	 
	obj = $(this);
	obj.each(function(){
		
		if($(this).attr('placeholder') != ""){
			if($(this).attr('type') == 'password'&& !$(this).hasClass('clone-parent')){
				val = $(this).attr('placeholder');
				$(this).parent().prepend('<input class="clone" placeholder="'+val+'" type="text" value="'+val+'" style="color:gray;">');
				$(this).addClass('clone-parent');
				$(this).val('');
				$(this).hide();
				vvPlaceHolderFocus('.clone');
				vvPlaceHolderBlur('.clone');
			}else{
				if($(this).val() == ""){
					$(this).css('color','gray').val($(this).attr('placeholder'));
				}
			}
				/*
				if($(this).attr('type') == 'password' && !$(this).hasClass('clone-parent')){
					clone = $(this).clone();					 
					$(this).hide();
					$(this).addClass('clone-parent');
					clone.attr('name','');
					clone.attr('id',clone.attr('id')+'-clone');
					clone.attr('clid',clone.attr('id'));
					clone.attr('class','clone');				
					clone.attr('type','text');	
					clone.css('border','2px solid green');
					$(this).css('border','2px solid red');	
					$(this).parent().prepend(clone);
				}
				*/
				
			
		}
		
		vvPlaceHolderFocus(this);	
		vvPlaceHolderBlur(this);
			
	});
	
	function vvPlaceHolderFocus(obj){
		 $(obj).focus(function(){
			if($(obj).hasClass('clone')){
				vvDebugg('clone focus');
				$(obj).parent().find('.clone-parent').show().val('').focus();
				$(obj).hide();
			}else{
				if($(obj).val() == $(obj).attr('placeholder')){
					$(obj).css('color','').val('');
				}
			}
		 });
	}
	function vvPlaceHolderBlur(obj){
		 $(obj).blur(function(){
			if($(obj).hasClass('clone-parent')){
				if($(obj).val() == ""){	
					$(obj).parent().find('.clone').show();
					$(obj).hide();
				}
			}else{
				if($(obj).val() == ""){				 
					$(obj).css('color','gray').val($(obj).attr('placeholder'));
				}
			}
			
		});
	}
}



$.fn.autoGrowInput = function(o) {
            
	o = $.extend({
		maxWidth: 1000,
		minWidth: 0,
		comfortZone: 70
	}, o);
	if($("#autoGrowInput-test").get(0) == undefined){
		
		$("#body").append('<div id="autoGrowInput-test"></div>');
		$('#autoGrowInput-test').css({
				position: 'absolute',
				top: -9999,
				left: -9999,
				width: 'auto',
				
				whiteSpace: 'nowrap'
			});
			 
	}
	
	
	this.filter('input:text').each(function(){		
		var minWidth = o.minWidth || $(this).width(),
			val = '',
			input = $(this),			
			check = function() {
				if (val === (val = input.val())) {return;}
				testSubject = $("#autoGrowInput-test");
				// Enter new content into testSubject
				var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,'&nbsp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				testSubject.html(escaped);
				 
				// Calculate new width + whether to change
				var testerWidth = testSubject.width(),
					newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
					currentWidth = input.width(),
					isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
										 || (newWidth > minWidth && newWidth < o.maxWidth);
				
				 
				
				// Animate width
				if (isValidWidthChange) {
					input.width(newWidth);
				}
				
			};
			
		
		
		$(this).bind('keyup keydown blur update', check);
		
	});
	return this;
};
   
