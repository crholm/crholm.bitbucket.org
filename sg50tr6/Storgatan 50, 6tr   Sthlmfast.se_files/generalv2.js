var DText = new Array();
var vvUPText = new Array();
var allText = new Array();
var MText = new Array();
var $ac = new Array();
var ajaxBoxMove = false;
var noScrollHistory  = false;
var currentPage = new Array();

var popUpWindow = null;

function vvSizer(){
	var h = $(window).height();
	var dh = $(document).height();
	var w = $(window).width();
	var s = $(window).scrollTop();
	$(document).trigger('vvSizer');
	$(document).trigger('vvSizerExtra');
	$("._col-long").vvShorten();
}

function vvHeroImage(img){
	if($("#hero-image").get(0) == undefined){
		$(body).append('<div id="hero-image" class="hero-image"><div class="_hold"></div></div>');
	}
	 $('#hero-image ._hold').anystretch(img);
}
	
function vvMobileMenu(){
	vvDebugg('Depricated, see jQuery vvMobileMenu');		
}

function vvShowBackBtn(){
	$("#backbtn").show();
}
function vvHideBackBtn(){
	$("#backbtn").hide();
}


/*
 * Edit in progress
 * vvEditInProgress = true
*/ 
var vvEditInProgress = false;
function vvCheckClose(){
  	if(vvEditInProgress){
    	return "Det finns ändringar som inte är sparade. Lämna sidan ändå?";
	}
}
window.onbeforeunload = vvCheckClose;

var vvCurrentInput = null;
var vvShareTimer = null;
var vvShareId = null;


function vvCheckMenuChildren(){
	$(".header-container").removeClass('children-aktiv');
	$('.aktiv').each(function(index, element) {
		if( $('.childof-'+$(element).attr('idx')).length>0){
			$(".header-container").addClass('children-aktiv');
		}
	});
}

function vvInit(){
	 
	//vvDebugg('vvInit');
	if($(window).width()<400){
		if (typeof FB !== "undefined") {		
			FB.XFBML.parse();
		}
		if (typeof twttr !== "undefined") {
			twttr.widgets.load();
		}
		if (typeof gapi !== "undefined") {
			gapi.plusone.go();
		}
	}
	
	if (typeof Holder !== "undefined") {	
		// Holder.js
		Holder.run();
	}
	
	
	vvCheckMenuChildren();
	
	// vvAjax Navigation init
	if ("pushState" in history) {
		//vvDebugg('pushState -> '+settings['URL_PUSH']);
		if(settings['URL_PUSH']){
			$('a').each(function(index, obj) {
				 						
				if($(obj).attr('idx') != undefined){
					if(currentPage['id'] == $(obj).attr('idx')){
						if($(obj).hasClass('child')){
							$("#menu-a-"+$(obj).attr('child-of')).addClass('aktiv');							
						}
						$(obj).addClass('aktiv');	
						vvDebugg('Add class aktiv 1. Current page '+currentPage['id']);
						document.title = HtmlDecode(currentPage['title']+" | "+settings['TITLE']);						
					}
				}
				
				if( $(obj).hasClass('popup') ){
					$(obj).unbind('click').click(function(e) {
						e.preventDefault();
						window.open(HtmlDecode($(this).attr('url')),"popUpWindow","width=700,height=500,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");
					});
				}
				
				if($(obj).hasClass('ajax')){						
					$(obj).bind('click',function(e) {
						 if($(obj).hasClass('loadPage')){	
							loadPage = false;
						}else{
							loadPage = true;
						}
						if($(this).attr('title') != undefined){
							var title = $(this).attr('title');	
						}else{
							var title = $(this).text();		
						}
									 
						$(document).trigger('vvPageChange');						 
						var url = this.href;
						lastTitle = document.title;							
						currentPage['title'] = title;
						currentPage['id'] = $(this).attr('idx');											
						pushScrollHistory[lastTitle] = $(window).scrollTop();						
						pushState({ID:""+$(obj).attr('id'),Title:title,LoadPage:loadPage}, title, url);	
					});
				}else if(!$(obj).hasClass('noAjax') && $(obj).attr('href') != '#menu' && $(obj).attr('target') != '_blank'){
					 					
					$(obj).unbind('click').bind('click',function(e) {
						
						
						$(document).trigger('vvClick');
						 
						if(this.href.match(/\#/)){
							vvDebugg('Hash');
							hash = this.href.split("#");
							$(document).trigger('vvHash');
							if($("#"+hash[1]).get(0) != undefined){			
								$("#"+hash[1]).show();
								$("."+hash[1]).hide();
								$(body).stop().animate({ scrollTop: $("#"+hash[1]).position().top});
							}
							return;
						}
						if($(this).attr('href').match(/\http/)){
							vvDebugg('Http');
							$(document).trigger('vvOut');
							return;
						}
						 
						$(this).parent().parent().find('.aktiv').removeClass('aktiv');
						if(!$(obj).hasClass('child') && $(obj).attr('child-of')){							 
							$('.aktiv').removeClass('aktiv');
							$(obj).addClass('aktiv');
							vvDebugg('Add class aktiv 2');						
							$('.child').hide();// Göm alla children						
							$('.childof-'+$(obj).attr('idx')).show(); // Visa children som aktiv har
							
						}else{
							$(obj).addClass('aktiv');
							vvDebugg('Add class aktiv 3');
						}
						
						vvCheckMenuChildren();
						
						$(document).trigger('vvPageChange');
						e.preventDefault();
						 
						
						if($(obj).hasClass('child')){						 
							var title = $("#menu-a-"+$(obj).attr('child-of')).attr('title')+' | '+$(this).attr('title');
						}else{
							if($(this).attr('title') != undefined){
								var title = $(this).attr('title');	
							}else{								 
								var title = $(this).text();		
							}
						}
						
						if($(obj).hasClass('loadPage')){	
							loadPage = false;
						}else{
							loadPage = true;
						}
						
						
						var url = this.href;
						lastTitle = document.title;	
					 
						currentPage['title'] = title;
						currentPage['id'] = $(this).attr('idx');
						vvDebugg('currentPage id = '+currentPage['id']);					
						pushScrollHistory[lastTitle] = $(window).scrollTop();						
						pushState({ID:""+$(obj).attr('id'),Title:title,LoadPage:loadPage}, title, url);	
					});
				}else{
					
				}
			});
		}
	}
	
	
	
	if(location.href.match(/\#/)){		
		 
		hash = location.href.split("#");
		$(document).trigger('vvHash');
		if($("#"+hash[1]).get(0) != undefined){			
			$("#"+hash[1]).show();
			$("."+hash[1]).hide();
			$(body).stop().animate({ scrollTop: $("#"+hash[1]).position().top-20});
		}		
	}
	
	
	
	$("input, textarea, select").focus('keydown',function(e) {
		vvCurrentInput = $(this);
		$(document).unbind('vvSave').bind('vvSave',function(){
			vvDebugg('Save');
			vvCurrentInput.parent().parent().parent().find('.save').trigger('click');
		});
		$(document).unbind('vvEnter').bind('vvEnter',function(){
			vvDebugg('vvEnter');
			vvCurrentInput.parent().parent().find('.enter').trigger('click');
			vvCurrentInput.parent().parent().parent().parent().find('.enter').trigger('click');
			$(document).trigger('vvExtraEnter');
		});
       
    });
	$(".master-form input, .master-form textarea, .master-form select").unbind('keydown').bind('keydown',function(e) {		 
		if(!$(this).hasClass('vvMediaSelect') && vvEditInProgress == false){
        	vvEditInProgress = true;
			vvDebugg('vvEdit in progress, make edit alert.');		 
		}
    }); 
	
	
	
	// Ställer in menyn för children
	$(".menu-item").each(function(index, element) {
		childof = parseInt($(this).attr('child-of')); 
		 
        if(childof != 0 && !$(this).hasClass('child')){
			$(this).addClass('child');
			 
			$(this).parent().detach().appendTo('#navbar-meny-children-'+$(this).attr('child-of')+'');			 
			$('.childof-'+$(".menu-item.aktiv").attr('idx')).show();	
		}
    });
	
	$(".navbar-meny-children").each(function(index, element) {
        if($(this).children().length == 0){
			$(this).remove();
		}
		
    });
	
	
	$(".like").unbind('click').bind('click',function(){
		id = parseInt($(this).parent().attr('idx'));
		t = $(this).parent().attr('type');
		$(this).addClass('aktiv');
		vvDebugg('Add class aktiv 4');
		vvGetData(path+'?init=vvLike',{idx:id,type:t},function(data){			 
			if(data.success == 1){
				
			}
		});
	});
	$(".facebook-share").unbind('click').bind('click',function(e){
		e.preventDefault();	
		url = $(this).parent().data('url');
		if(url == ""){			
			url = window.location.href;
		}		 
		var url = 'https://www.facebook.com/sharer/sharer.php?u='+url;
		window.open(url, 'facebook', 'menubar=no,toolbar=no');		
	});
	$(".pinterest-share").unbind('click').bind('click',function(e){
		e.preventDefault();	
		url = $(this).parent().data('urlimage');
		if(url == ""){			
			url = window.location.href;
		}		 
		var url = 'http://pinterest.com/pin/create/button/?url='+url;
		window.open(url, 'pinterest', 'menubar=no,toolbar=no');		
	});
	$("._share_facebook").unbind('click').bind('click',function(e){
		e.preventDefault();	
		
		if($(this).attr('fb') != undefined && $(this).attr('fb') != ""){
			item_url = $(this).attr('fb');
		}else{
			item_url = window.location.href;
		}
		 
		var url = 'https://www.facebook.com/sharer/sharer.php?u='+item_url;
		window.open(url, 'facebook', 'menubar=no,toolbar=no');		
	});
	
	$(".share").unbind('click').bind('click',function(e){
		if( $('#_share_box').get(0) == undefined){
			$("#body").append('<div id="_share_box"><a class="facebook noAjax" target="blank" href="#"></a><a class="twitter noAjax" target="_blank" href="#"></a></div>');	
		}
		$("#_share_box").attr('type',$(this).parent().attr('type'));
		$("#_share_box").attr('idx',$(this).parent().attr('idx'));
		var offset = $(this).offset();
		$("#_share_box").css({top:offset.top,left:offset.left-100});
		$("#_share_box").show();
		vvShareId = $(this);
		clearTimeout(vvShareTimer);
		$(".facebook").unbind('click').bind('click',function(e){
			 
			id = parseInt($(this).parent().attr('idx'));
			t = $(this).parent().attr('type')+'_facebook';
			$(this).addClass('aktiv');
			var url = currURL+id+'/';
			$(this).attr('href','https://www.facebook.com/sharer/sharer.php?u='+url);
			vvGetData(path+'?init=vvShare',{idx:id,type:t},function(data){
				vvTrackEvent('twitter-share',"Share.facebook.content");
				vvShareId.addClass('aktiv');
			});			
		});
		$(".twitter").unbind('click').bind('click',function(e){
			 
			id = parseInt($(this).parent().attr('idx'));
			t = $(this).parent().attr('type')+'_twitter';
			$(this).addClass('aktiv');
			var url = currURL+id+'/';
			$(this).attr('href','https://twitter.com/intent/tweet?text=&url='+url);
			vvGetData(path+'?init=vvShare',{idx:id,type:t},function(data){			
				vvTrackEvent('twitter-share',"Share.twitter.content");
				vvShareId.addClass('aktiv');
			});
		});
		vvShareTimer = setTimeout(function(){
			$("#_share_box").hide();
		},3000);
	});
	
	
	// vvMenuClick
	$("#admin_menu_hold a").click(function(){		 
		$("#admin_menu").stop().animate({width:0});
	});
	
	/* VVMEDIA BOX ACTIONS */
	
	$(".media-file").unbind('click').bind('click',function(){
		if(!isCtrlPressed){
			$(".media-file").removeClass('aktiv');
		}
		$(this).addClass('aktiv');
		vvHandleSelect($(this));
	});
	$("#vvInsertFile").unbind('click').bind('click',function(){			
		 
		preview = $("#"+vvSelectOrgin).parent().parent().find('._preview');
		
		var tempVal = null;
		
		preview.find('._f').each(function(index, element) {
            imgid = $(this).find('img').attr('idx');
			inArray = false;
			for(i in vvSelectObj){
				 
				if(imgid == vvSelectObj[i].idx){
					inArray = true;
				}
			}
			if(!inArray){
				$(this).remove();	
			}
        });
		
		$(vvSelectObj).each(function(){
			 
			if($("#f_"+this.idx).get(0) == undefined){
				preview.append('<div class="_f dib"><input class="_caption" idx="f_'+this.idx+'"><img src="'+this.src+'" id="f_'+this.idx+'" idx="'+this.idx+'"></div>')
			}
			if(tempVal == null){
				tempVal = this.idx;
			}else{
				tempVal = tempVal+','+this.idx;
			}		
		});
		
		$("#"+vvSelectOrgin).val(tempVal);
		vvCloseBox();
	});
	
	
	//Filupader
	if($(".file-uploader").get(0) != undefined){
		$(".file-uploader").each(function(index, element) {
            if(BrowserDetect.version<5 && BrowserDetect.browser == 'Safari'){
				$(this).html('<div class="qq-uploader">Du måste använda en annan webläsare</div>');
			}else{
				 
				vvFileUploader($(this),'vvFileUpload',DText);
			}
        });
		
	}
	//Filupader
	if($("#user-file-uploader").get(0) != undefined){
		if(BrowserDetect.version<6 && BrowserDetect.browser == 'Safari'){
			$('.file-uploader').html('<div class="qq-uploader"></div>');
		}else{
			DText.uploadButton = $("#user-file-uploader").attr('laddbtn');
			vvUserUploader('user-file-uploader','vvFileUpload',DText,$("#user-file-uploader").attr('multi'));
		}
	}
	
	//Overbuttons
	$("#vvOverlay,#closebtn,.closebtn").unbind('click').bind('click',function(){
		vvCloseBox();
	});
	
	// VVSUBMIT
	$(".form").unbind('subtmit').bind('submit',function(e) {
		e.preventDefault();	
		vvSubmit(this);			
	});
	// VVSUBMIT
	$(".submit").unbind('click').bind('click',function(e) {
		e.preventDefault();	
		vvSubmit(this,e);
		
	});
	// vvSubmitEnd
	
	$('.mediaBox').unbind('click').bind('click',function(e) {
		img = $(this).attr('large');
    	vvHtmlBox('<img src="'+img+'" style="width:100%;">');
    });
	
	$("#backbtn").unbind('click').bind('click',function(e){
		e.preventDefault();	
		history.back();
	});
	
	
	
	
	
	
	/* POST EDIT FUNCTIONS */
	
	$('.master-form-toggle').unbind('click').click(function(e) {
		$('.master-form').slideToggle('slow');
	});
	$(".imageSelect,.mediaSelect").unbind('click').click(function(e) {
		var t = $(this).find('.vvMediaSelect');	
		vvAjaxBox('vvMediaBox&type='+t.attr('f_type')+'&rel='+t.attr('rel')+'&id_rel='+t.attr('id')+'&multi='+t.attr('multi')+'&value='+t.val());
		 
	});
	$(".vvMediaSelect").unbind('click').click(function(e) {			 
		vvAjaxBox('vvMediaBox&type='+$(this).attr('f_type')+'&rel='+$(this).attr('rel')+'&id_rel='+$(this).attr('id')+'&multi='+$(this).attr('multi')+'&value='+$(this).val());
	});
	
	
	
	//$("select").vvSelector(); 
	

	
 
	$('._sortable').sortable();
	if(vvImageSorterTimer == null){
		if($('.boxFormMedia').get(0) != undefined){
			vvImageSorterTimer = setInterval('vvImageSorter()',1000);
		}
	}
		
		
	$(".login").unbind('click').bind('click',function(e) {
		e.preventDefault();
		$(".boxHolder").show();
	}); 
	$(".boxHolder").mouseleave(function(e) {
		$(this).hide();
	});
	$(".dropdown-menu").unbind('click').bind('click',function(e) {
		$(this).parent().find('._list').toggle();
	});
	$(".dropdown-menu ._list").mouseleave(function(e) {
		$(this).parent().find('._list').hide();
	}); 	
	
	
	setTimeout(function(){
		// Lägg till scrolltop on reload
		pushScrollHistory[document.title] = $(window).scrollTop();
		 
	},500);
	
	// vvAjax Navigation init FOR objects (_object_menu)
	if ("pushState" in history) {
		if(settings['URL_PUSH']){
			$("._object_menu ._a").each(function(index, obj) {
				$(obj).unbind('click').click(function(e) {
					e.preventDefault();
					var title = $(this).text();
					var url = this.href;							
					pushState({ID:""+$(obj).attr('id'),Title:title,vScrol:$(window).scrollTop()}, title, url,false);	
					objectActions($(this).attr('action'));
				});				
			});
		}
	}
	
	
	
	
	// Input comma functions	 
	$('._input-comma input').autoGrowInput({
		comfortZone: 25
	});
				 
	$("._input-comma ._ins").unbind('focus').bind('focus',function(){
		icObj = $(this).parent().parent();
		icObj.find('._placeholder').hide(); 
    })
	$("._input-comma ._ins").unbind('blur').bind('blur',function(){
		icObj = $(this).parent().parent();		 
		icVal = $(this).val();
		$(this).val('');
		if(icVal != ""){
			icObj.find('._commas').append('<div class="_comma-vals">'+icVal+'<i class="_del"></i></div>');
			$("._comma-vals i").unbind('click').bind('click',function(){
				ssdad = $(this).parent().parent().parent().parent();	
				$(this).parent().remove();				
				vvInputCommaActions(ssdad);
			});
		}
		vvInputCommaActions(icObj);
    });
	$("._input-comma ._ins").keydown(function(e) {
		icObj = $(this).parent().parent();	
		if($(this).hasClass('_auto')){
			 
		}
        if(e.keyCode == 13){
			$(this).blur();
			$(this).focus();
		}
		if(e.keyCode == 8){
			if($(this).val() == ""){
				icObj.find('._commas ._comma-vals:last').remove()
			}			
		}
    });
	
	$("._input-comma-value").each(function(index, element) {
		icObj = $(this).parent();
		
        if(!$(this).hasClass('_loaded')){
			icObj.find('._commas').html('');
			$(this).addClass('_loaded');
			val = $(this).val().split(",");
			if(val != ""){
			}
			 
			for(e in val){
				icVal = val[e];				 
				if(icVal != ""){
					icObj.find('._placeholder').hide(); 
					
					if(icVal.match(/\|/)){
						TestVal =icVal.split("|");
						icObj.find('._commas').append('<div class="_comma-vals"><span class="value" idx="'+TestVal[0]+'"></span>'+TestVal[1]+'<i class="_del"></i></div>');
					}else{
						icObj.find('._commas').append('<div class="_comma-vals">'+icVal+'<i class="_del"></i></div>');
					}
					$("._comma-vals i").unbind('click').bind('click',function(){
						ssdad = $(this).parent().parent().parent().parent();	
						$(this).parent().remove();				
						vvInputCommaActions(ssdad);
					});
					
				}
			}
		}
    });
	
	 
	
	/*
	$("._complete").unbind('click').bind('click',function(e){
		
		if($(this).attr('confirm') != undefined){
			confrim = $(this).attr('confirm');
		}else{
			confrim = 'Perform action?';
		}		 
		if($(this).hasClass('confirm')){
			if(!confirm(confrim)){
				return;
			}
		}
		var obj =eval("("+$(this).attr('parms')+")");
		 
		$("#"+obj.id).css('opacity','0.1');
		$("#"+obj.id).find('.status').val('2');
		$("#"+obj.id).find('input').prop('readOnly', true);
		$("#"+obj.id).find('.slider').remove();
	});
	
	
	$("._completed").each(function(index, element) {
			 
        $(this).find('._complete').removeClass('confirm').trigger('click');
    });
	*/
	$("._complete-task").find('input,textarea').prop('readOnly', true);
	$("._complete-task").find('.slider').remove();
	
	
	$(".copyable-btn").each(function(index, element) {
		if(copyableElements[$(this).attr('idx_obj')] == undefined){
			copyableElements[$(this).attr('idx_obj')]  = $("#"+$(this).attr('idx_obj')).html();
			$("#"+$(this).attr('idx_obj')).remove();
			 
		}
		$(this).unbind('click').bind('click',function(){
			obj = copyableElements[$(this).attr('idx_obj')];			 
			$("#"+$(this).attr('idx_to')).append(obj);
			vvSliderSet();
			$(document).trigger('vvInit');			 
			
		});
	});
	vvSliderSet();
	 
	
	$('._showObj').unbind('click').bind('click',function(){
		$("#"+$(this).attr('idx')).show();
		$("#"+$(this).attr('idx')).mouseleave(function(e) {
            $(this).hide();
        });
	});
	
	
	$(".vvAjaxBox").unbind('click').bind('click',function(e){
		e.preventDefault();
		var obj =eval("("+$(this).attr('parms')+")");
		vvAjaxBox($(this).attr('action'),obj);
	});
	
	var query = getQueryParams(document.location.search);
	if(query.option != undefined){
		var key = "."+query.option+"-"+query.id;
		if(optionsUsed[key] == undefined){
			$("."+query.option+"-"+query.id).trigger('click');
			optionsUsed[key] = key;	
		}
	}
	if(query.light != undefined){
		var key = "."+query.light+"-"+query.id;
		
		if(optionsUsed[key] == undefined){
			 
			$(key).addClass('_light');
			optionsUsed[key] = key;	
		}
	}
	
	if($("._datepicker").get(0) != undefined){
		if(!$("._datepicker").hasClass('timepicker')){		
			$("._datepicker").addClass('timepicker');
			$("._datepicker").datetimepicker({lang:'se'});
		}
	}
	
	
	

	$(".cal_arrow").unbind('click').bind('click',function(){
		$("._calender ._inside").vvGetData({
			url:'/?init=vvCalender',
			parms:{cMonth:$(this).attr('m')},
			callback:function(data){
				$("._calender ._inside").html(data);
				$(document).trigger('vvInit');
				$(document).trigger('vvCalender');
			}
		});
	});

	$(document).trigger('vvInitExtra');
	
	$(".timeago").each(function(){
		
		$(this).timeago();
	});
	
	$(".pipe").each(function(index, element) {		
        if(!$(this).hasClass('pipe-loaded')){			
			$(this).addClass('pipe-loaded');			
			pipes.push({
				obj:$(this),
				action:$(this).data('action')
			});
			 
		}
    });
	
			
}// VVINIT SLUT

var pipes = new Array();
function vvPipes(){	
	for(var e in pipes){
		$(pipes[e].obj).vvGetData({url:'/?init='+pipes[e].action});		 
	}
}

var timeBetweenCalls = 1000; // Milliseconds
var numberOfCalls = 10;
var timesRun = 0;
var startTime = new Date().getTime();
$(document).ready(function(e) {
	(function vvPipesRun () {
		var now = new Date().getTime();  
		if (timesRun <= numberOfCalls) {
			 
			timesRun++;
			if(pipes.length>0){
				vvPipes();			
				timeBetweenCalls = timeBetweenCalls+10000;
			}else{
				timeBetweenCalls = 5000;
			}
			setTimeout(vvPipesRun, timeBetweenCalls - ((new Date().getTime() - startTime) % timeBetweenCalls));
		}else{
			timesRun = 0;
			timeBetweenCalls = 5000;
			 
		}
	})();    
});



var $input = new Array();
var optionsUsed = new Array();

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}



function vvSliderSet(){
	$(".slider").each(function(){	
		if(!$(this).hasClass('noUi-target')){
			  
			if($(this).parent().parent().find('input').val() == ""){
				start = 1;
			}else{
				start = $(this).parent().parent().find('input').val();
			}
			$(this).noUiSlider({
				 range: [1, 10],
				 start: start,
				 handles: 1	,
				 serialization: {
					resolution: 1,
					to: $(this).parent().parent().find('input')
				}		
			});
		}		
	});
}

var copyableElements = new Array();

function vvInputCommaActions(icObj){
	var jsejf = null;
	icObj.find('._commas ._comma-vals').each(function(index, element) {	
 
		if($(this).find('span').hasClass('value')){
			
			if(jsejf == null){			 
				jsejf = $(this).find('span').attr('idx')+'|'+$(this).text();
			}else{			 
				jsejf += ','+$(this).find('span').attr('idx')+'|'+$(this).text();
			}
		}else{
			if(jsejf == null){			 
				jsejf = $(this).text();
			}else{			 
				jsejf += ','+$(this).text();
			}	
		}
    });
	icObj.find('._input-comma-value').val(jsejf);
}


 

function vvAddHtml(html){
	obj = $(HtmlDecode(html));
	if($("#"+obj.attr('id')).get(0) == undefined){
		 
		$(body).prepend(obj);
		$("#"+obj.attr('id')).slideToggle('slow');
		$(document).trigger('vvInit');
	}
}

var vvTimerVar = false;  
var vvTimerInter = null;

function vvTimer(time,update,complete) {				
	var start = new Date().getTime();
	if(vvTimerInter != null){
		clearInterval(vvTimerInter); 
		vvTimerInter = null;
		vvTimerVar = false;
		$('.timeout').removeClass('aktiv');
	}
	vvTimerInter = setInterval(function() {		 
		var now = time-(new Date().getTime()-start);	
		if( now <= 0) {
			clearInterval(vvTimerInter);
			complete();
		}else{ 
			update(Math.floor(now/1000));
		}
	},100); // the smaller this number, the more accurate the timer will be
}
 

 
 
var vvImageSorterTimer = null;
function vvImageSorter(){
	 
	//vvDebugg(' - vvImageSorter . in generaj.js');
	$('.mediaSelect').each(function(index, element) {			 
			val = "";
			$(element).parent().find('._sortable ._f').each(function(index, element) {
				img = $(this).find('img');
				caption = $(this).find('._caption');
				if($(img).attr('idx') != undefined){
					if(val == ""){
						val = $(img).attr('idx')+':'+caption.val();
					}else{
						val = val+'|'+$(img).attr('idx')+':'+caption.val();
					}
				}
			});
			if(val != ""){
				$(this).find('input').val(val);
			}
			
	});
}
 
function vvResetAjax(){
	 
	
	var _Data = new Array();
	var exclude_arr = new Array();	
	var vvSort = null;
	var vvGettingData = false;
	var listDiv = "";
	var teMplateDiv = "";
	var teMplateName = "";
	var initFunc = '';
	var call = "";
	$(".listToggler li").unbind('click');
	$("#wrapper").removeAttr('class');
}


var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
 
if (document.attachEvent){ //if IE (and Opera depending on user setting)
    document.attachEvent("on"+mousewheelevt, function(e){ $(document).trigger('vvScroll',e); });
} else if (document.addEventListener){
    document.addEventListener(mousewheelevt, function(e){ $(document).trigger('vvScroll',e); }, false);
}


$(window).scroll(function(){	
	
});

$(document).bind('vvScroll',function(){
	pushScrollHistory[document.title] = $(window).scrollTop();
	 
});


function vvGetData(aUrl,parms,callback){
		
		vvDosubmit = true;		
		$.ajax({
			url:aUrl,
			data:parms,
			type:'POST',
			dataType:"JSON",
			beforeSend: function(){
				vvLoadShow();
			},
			complete: function(data){					
				vvDosubmit = false;
				vvLoadHide();
				
			},
			success: function(data){				 
				callback(data);
				$(document).trigger('vvGetDataCollected');
			}
		});	 
}


// ---------------- PUSH STATE NAVIGATION ------------------ //
$(window).load(function(e) {
	if ("pushState" in history) {
		if(settings['URL_PUSH']){
			settings['URL_PATH'] = location.href;
			window.onpopstate = function(e) {
				if(vvEditInProgress){
					if(!confirm("Det finns ändringar som inte är sparade. Lämna sidan ändå?")){
						return;
					}
				}
				if(settings['URL_PATH'] != location.href){	
					if(location.href.match(/\#/)){
						hash = location.href.split('#');
						$("#"+hash[1]).show();		
						$("."+hash[1]).hide();				 
						$(document).trigger('vvHash');						 
					}else{
						 
						
						if(e != undefined){
							if(e.state != undefined){
								e.state.LoadPage = true;
							}else{
								e.state = {LoadPage:true};
							}
						}
						
						 
						vvLoadPage(location.href, e.state, true);											
					}
				}
			};
		}
	}
});

var pushScrollHistory = new Array()
function pushState(data, title, url) {
	 
	if ("pushState" in history) {
		if(settings['URL_PATH'] != url){			 
			history.pushState(data, title, url);
			vvLoadPage(location.href, data, false);			
		}
	}else{
		top.location = url;		
	}
}

var lastPageReload = null;
var lastPageTime = 2;

function vvLoadPage(url, state, history){
	 
	vvDebugg('-----------------------------');	 
	$(document).trigger('vvPageChange',state);	
	if(!settings['URL_PUSH']){
		vvDeugg('URL_PUSTH false');
		return;
	}
	vvHideBackBtn();
	vvLoadShow();
	settings['URL_PATH'] = url;	
	 
	 
	if(document.location.pathname == "/"){	
		$('.aktiv').removeClass('aktiv');	
		document.title = HtmlDecode(settings['TITLE']);	
		vvDebugg('Startsidan');
		currentPage['id'] = 0; 
	}else{
		if(state != null){			 			
			if(state.Title != undefined){				
				document.title = HtmlDecode(state.Title+" | "+settings['TITLE']);
			}else{				
				document.title = HtmlDecode(settings['TITLE']);
			}			
			if(state.ID != undefined){				 
				$("#"+state.ID).parent().parent().find('.aktiv').removeClass('aktiv');						
				$("#"+state.ID).addClass('aktiv');		
				currentPage['id'] = state.ID; 
			}
		}
	}
	
	// Aktuell sida title
	currentPage['title'] = document.title;
	
	
	 
	
	// Track url
	vvTrackUrl(url);
	
	// Om sidan inte ska ändra innehåll	
	if(state != null){		
		if(state.LoadPage == false){
			if(history){
				if($("#"+state.ID).get(0) != undefined){
					$("#"+state.ID).trigger('click');
				}
			}
			vvLoadHide();
			vvEditInProgress = false;			
			return;
		}
	}
	
	$.post(url,{vvAjaxRequest:true},function(data){		
		 
		vvEditInProgress = false;
		vvResetAjax();
		$('nav#menu').trigger('close');
		vvLoadHide();		
		$("#content").html(data);
		hash = url.split('#');
		
		if($("#"+hash[1]).get(0) != undefined){			
			$(body).stop().animate({ scrollTop: $("#"+hash[1]).position().top});
		}else{	
				
			if(pushScrollHistory[document.title] == undefined || noScrollHistory == true){
				$(body).stop().animate({ scrollTop: "0px" });
			}else{
				$(body).stop().animate({ scrollTop: pushScrollHistory[document.title]+"px" });
			}
		}		
		$(body).trigger('vvInit');
		
	});
	
}


function vvTrackEvent(url,vvE){
	if(typeof _gaq !== 'undefined'){
		_gaq.push(['_trackEvent', vvE, 'link',url]);
		vvDebugg(url);
	}
}
function vvTrackUrl(url){
	 
	if(typeof _gaq !== 'undefined'){
		url = url.replace(settings['fullurl'],'');
		vvDebugg(url);
		_gaq.push(['_trackPageview', url]);
	}
}

// ---------------- VVBOX functions ------------------ //
function vvChackArrows(){
	
	if($("#arrow-left").get(0) == undefined){
		vvDebugg('arrows');
		$("#vvBox").append('<div id="arrow-left" class="arrow" idx="1"></div><div id="arrow-right" class="arrow" idx="-1"></div>');
		$('#arrow-left').unbind('click').bind('click',function(){
			$(document).trigger("vvArrowLeft");
		});
		$('#arrow-right').unbind('click').bind('click',function(){
			$(document).trigger("vvArrowRight");
		});
		vvInit();
	}
}
function vvRemoveArrow(){
	$(".arrow").remove();
}
function vvCheckBox(){
	setTimeout(function(){
		ajaxBoxMove = false;
	},300);
	
	if($("#vvBox").get(0) == undefined){
		$("#body").append('<div id="vvBox"><div id="closebtn"></div><div id="ajaxhtml"></div><div id="ajaxloading_n"><img src="'+template+'/images/loading.gif" /></div></div>');
		$("#body").append('<div id="vvOverlay"></div>');
		$("#vvOverlay").hide();
		$("#vvBox").hide();
		
		vvInit();
	}
}


function vvCloseBox(){
	settings['_vvBoxStatus'] = 'closed';
	if(settings['URL_PATH'] != null){
		 
		temp =  settings['URL_PATH'].split('#');
		settings['URL_PATH'] =temp[0]+'#s';
		openImage = null;
		//top.location = settings['URL_PATH'];
	}
	$("#vvOverlay").hide();
	$("#vvBox").hide();
	$("#vvBox #ajaxhtml").html('');
	$("#_body").removeClass('_filter-blur');
	$(document).trigger('vvBoxClosed');
	 
}
		
		
function vvAjaxBox(f,parms){
	vvTrackEvent(f,'ajax.vvAjaxBox');
	if(parms == undefined){
	 	parms = {clean:true,arrow:false,overlay:true,cc:null,start:{x:0,y:0}}
	}else{
		 
		if(parms.clean == undefined){
			parms.clean = true;
		}
		if(parms.cc == undefined){
			parms.cc = null;
		}
		if(parms.overlay == undefined){
			parms.overlay = true;
		}
		if(parms.arrow == undefined){
			parms.arrow = false;
		}
		if(parms.start == undefined){
			parms.start = {x:0,y:0};
		}
		if(parms.blur == undefined){
			parms.blur = false;
		}else{
			if(parms.blur){
				$("#_body").addClass('_filter-blur');
			}
		}
	}
	
	
	
	vvCheckBox();
	if(parms.arrow){
		vvChackArrows();
	}
	
	$("#ajaxloading").fadeIn();
	
	$("#vvBox").removeAttr('class');
	if(parms.cc != null){		
		$("#vvBox").addClass(parms.cc);
	}
	
	
	
	if(parms.clean == true){
		ajaxBoxMove = false;
		if(parms.start != null){	
			$("#vvBox").css({left:parms.start.x,top:parms.start.y});
		}else{
			$("#vvBox").css({top:0,left:0});
		}
	}else{
		ajaxBoxMove = true;
	}
	$("#vvBox #ajaxhtml").html('');
	
	$("#vvBox").fadeIn('fast');
	if(parms.overlay){
		$("#vvOverlay").fadeIn('slow');
	}
	
	 
	vvLoadShow();
	$.get(path+'?init='+f,parms,function(data){
		vvLoadHide();
		$("#ajaxloading").fadeOut();
		$("#vvBox #ajaxhtml").html(data);
		 $(document).trigger('vvInit');
	});
	settings['_vvBoxStatus'] = 'open-'+f;	
}

function vvHtmlBox(html){
	vvRemoveArrow();
	vvCheckBox();
	$("#ajaxloading").hide();
	$("#vvBox #ajaxhtml").html('');
	$("#vvBox").fadeIn('fast');
	$("#vvOverlay").fadeIn('slow');
	$("#vvBox #ajaxhtml").html(html);
	
	settings['_vvBoxStatus'] = 'open-html';	
}


// ---------------- other functions ------------------ //

function validEmail(e) {
	var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
	return String(e).search (filter) != -1;
}


function vvPermalink(str) {
	var d = {"å": "a","ä": "a","ö": "o","Å": "a","Ä": "a","Ö": "o"};
	 
	str = str.replace(/[åäöÅÄÖ]/ig, function(s) {   return d[s]; });
    return str.replace(/[^a-z0-9]+/gi, '-').replace(/^-*|-*$/g, '').toLowerCase();
}


var vvSendingImage = false;
var vvSend = false;
var vvCurrentForm;

function vvSubmit(This,e){
	
	$(This).trigger('vvClick');
	if(!vvDosubmit){
		 
		_form = $(This);			
		obj = $(This);
		obj.parent().addClass('_progress');
		idx = $(This).attr('idx');
		
		 
		if($("."+idx).length == 0){			 
			parms = {idx:idx};
		}else{
			parms = $("."+idx).vvSerialize();	
		}
		
		if($(This).hasClass('no-clear')){
			parms.noClear = true;
		}
		if(!$(This).hasClass('noreq')){
			for (var key in parms) {					 
				if(typeof parms[key] == 'string'){
					if(parms[key].indexOf('_alert') == 0){
						alert(decodeURIComponent(parms[key].replace('_alert','')));
						return;
					}
				}
				if(parms[key] == '_empty'){		
				 	
					vvDosubmit = false;	
					$(document).trigger('vvInit');		
					return;
				}
			}
		}
		if($(This).hasClass('parms_alter')){
			k = $(This).attr('parms_key');
			v = $(This).attr('parms_val');
			parms[k] = v;			 
		}
		
		$("."+idx).prop('readOnly', true);
		action = $(This).attr('action');
		aUrl = path+'?init='+action;
		
		if(action == 'special'){
			window.open(aUrl+'&'+parms);
			return;
		}
		
	 
		if($(This).hasClass('parms_att')){
			parms = eval("("+$(This).attr('parms')+")");	
		}
		
		 
		 
		if($(This).attr('confirm') != undefined){
			confrim = $(This).attr('confirm');
		}else{
			confrim = 'Perform action?';
		}		 
		if($(This).hasClass('confirm')){
			if(!confirm(confrim)){
				return;
			}
		}
		// Debugg		
		  
		vvDosubmit = true;
		$.ajax({
			url:aUrl,
			data:parms,
			type:'POST',
			dataType:"JSON",
			beforeSend: function(){
				$(document).trigger('vvSaveOnceEvent');
				vvMsgBox('Var vänligen vänta...','submit-loading',100000);
				vvLoadShow();
				
			},
			complete: function(data){
				$(document).trigger('submit-loading');
				vvLoadHide();
				vvDosubmit = false;
				$("."+idx).prop('readOnly', false);
				
			},
			success: function(data){
				vvEditInProgress = false;
				vvDebugg(data);
				if(data.captcha == "1"){
					$(".captcha_input").hide();
					$("#file-uploader").show();
				}
				if(data.action != undefined){
					DataAction = data.action.split('|');
					 
					for(var a in DataAction){
						action = DataAction[a];
						 
						vvDebugg(action);
						if(data.success != undefined){
							if(data.success){
								$(document).trigger('vvSuccess');
							}else{
								$(document).trigger('vvFalse');
							}
						}
						
						if(action == 'reload'){								
							window.location.reload();
						}
						if(action == 'timeoutreload'){	
							setTimeout(function(){							
								window.location.reload();
							},2000);
						}
						if(action == 'root'){
							top.location = "/";
						}
						if(action == 'hideshow' || action == 'showhide'){
							$(data.hide).hide();
							$(data.show).show();
						}
						
						if(action == 'popupurl'){
							if(data.url != "" && data.url != 'reload'){
								popUpWindow = window.open(HtmlDecode(data.url),"popUpWindow","width=700,height=500,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");
							}
						}
						
						if(action == 'move_to'){
							vvDebugg('DOit');
							clone = obj.parent().parent().clone();
							clone.attr('db',data.mote_to_id_t);
							clone.attr('cat',data.mote_to_id_t);
							obj.parent().parent().remove();
							$(data.mote_to_id).append(clone);								
							$(document).trigger('vvInit');
						}
						if(action == 'timeouturl'){
							if(data.url != "" && data.url != 'reload'){
								setTimeout(function(){
									top.location = HtmlDecode(data.url);
								},2000);
							}else{
								if(data.url == 'reload'){
									$(document).trigger('vvReload');
								}
							}
						}
						if(action == 'timeoutreload'){								 
							setTimeout(function(){
								top.location.reload();
							},2000);								 
						}
						
						if(action == 'vvbox'){
							if(data.url != ""){								 
								vvAjaxBox(HtmlDecode(data.url));							 
							}else{
								alert('Url is empty');
							}
						}
						
						if(action == 'url'){
							if(data.url != ""){
								top.location = HtmlDecode(data.url);
							}
						}
						
						if(action == 'msgbox_arr'){
							for(var e in data.msg){
								if(data.msg[e] != null){
									if(data.msg[e].body != null){
										vvMsgBox(HtmlDecode(data.msg[e].body),'msgbox',data.msg[e].time,data.msg[e].type);
									}else{
										vvMsgBox(HtmlDecode(data.msg[e]),'msgbox',2000,'warning');
									}
									
								}
							}
						}
						if(action == 'modal'){
							if(data.id != null){
								modal = $("#"+data.id);
								$("#"+data.id).remove();
								$("#body").append(modal);
								modal.modal('show');
							}
						}
						if(action == 'msg'){
							if(data.msg != null){
								 
								vvDebugg(HtmlDecode(data.msg.body));
								alert(HtmlDecode(data.msg.body));
							}
						}
						if(action == 'msgbox'){
							if(data.msg != null){
								if(data.msg.body != null){
									vvMsgBox(HtmlDecode(data.msg.body),'msgbox',data.msg.time,data.msg.type);
								}else{
									vvMsgBox(HtmlDecode(data.msg),'msgbox',2000,data.sys_response);
								}
							}
						}
						if(action == 'hide'){
							
							 $("."+idx).parent().fadeOut('fast');
						}
						if(action == 'curhide'){
							 $("."+idx).fadeOut('fast');
						}
						if(action == 'html'){
							 _form.html('<span>'+data.msg.body+'</span>');
						}
						
						if(action == 'fade'){
							$("."+idx).parent().parent().fadeTo(100,0.5);
							$("."+idx).parent().parent().find('textarea,input,select').prop('readOnly', true);
							$("."+idx).parent().parent().find('.btn').remove();
							
						}
						if(action == 'fadethis'){
							$("."+idx).fadeTo(100,0.5);								
							
						}
						if(action == 'remove'){
							 
							$("."+idx).parent().remove();							
							$("._item-"+idx).parent().remove();
							$(document).trigger('vvInit');
						}
						if(action == 'reloadajax'){
							$(document).trigger('vvReloadAjax');
						}
						if(action == 'reloadpage'){
							vvLoadPage(location.href,{},true);
							$(document).trigger('vvReloadPage');
						}
						if(action == 'timeout-reloadpage'){
							setTimeout(function(){
								vvLoadPage(location.href,{},true);
								$(document).trigger('vvReloadPage');
							},2000);
						}
						if(action == 'toggle'){
							$("."+idx).animate({height:0},500,function(){
								$(this).remove();
							});
							$(document).trigger('vvInit');
						}
						if(action == 'removethis'){
							$("."+idx).remove();
							$(document).trigger('vvInit');
						}
					}
					if(data.empty){							
						$("."+idx).each(function(index, element) {
							if($(this).attr('type') == 'text' || $(this).attr('password')){
								$(this).val('');
							}
						});
					}
					if(data.clear){							
						$("."+idx).val('');
						 
						$("._form").find('._preview').html('');
						$(".reset").trigger('click');
						$(document).trigger('vvPost');
					}
				}
			}
			
		});
	}
}

function arrayCompare(a1, a2) {
    if (a1.length != a2.length) return false;
    var length = a2.length;
    for (var i = 0; i < length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(typeof haystack[i] == 'object') {
            if(arrayCompare(haystack[i], needle)) return true;
        } else {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}


function getOffsetSum(elem) {
	var top=0, left=0;
	while(elem) {
		top = top + parseInt(elem.offsetTop);
		left = left + parseInt(elem.offsetLeft);
		elem = elem.offsetParent;
	}
	return {top: top, left: left};
}


function appendScript(filepath) {
    if ($('head script[src="' + filepath + '"]').length > 0){
        return;
	}

    var ele = document.createElement('script');
    ele.setAttribute("type", "text/javascript");
    ele.setAttribute("src", filepath);
    $('head').append(ele);
}

function appendStyle(filepath) {
    if ($('head link[href="' + filepath + '"]').length > 0){
        return;
	}

    var ele = document.createElement('link');
    ele.setAttribute("type", "text/css");
    ele.setAttribute("rel", "Stylesheet");
    ele.setAttribute("href", filepath);
    $('head').append(ele);
}



/* FILE UPLOAD _-------------------------------------------------------------------------------- */
var timeedObjects = new Array();
 
var handleComplete = function( id, fileName, data) {
	if(data.action == 'input' && data.success){
		 
		file = data.vv.file;
		 
		$("#"+data.div_id).find('.file_input').val(data.vv.post_id);
		$("#"+data.div_id).find('.file-uploader').hide();
		$("#"+data.div_id).append('<img src="'+path+file.folder+file.idx+'-70x70.png">');
		 
		 
	}else if(data.success){		  
		//Visa alert om klar
		//vvSendingImage = true;
		file = data.vv.file;
		$(".vvMediaLib").append('<div class="media-file" url="'+path+file.folder+file.idx+'-small.png" idx="'+data.vv.post_id+'"><div class="delete-file confirm submit parms_alter delete-image-"'+data.vv.post_id+'"" confirm="Vill du ta bort bilden?" parms_key="idx" parms_val=""'+data.vv.post_id+'""  idx="delete-image-"'+data.vv.post_id+'"" action="vvDeleteFile" ></div><img src="'+path+file.folder+file.idx+'-small.png" idx="'+data.vv.post_id+'"></div>');
		$('.qq-upload-success').each(function(index, element) {
            if($(this).attr('timeed') == undefined){
				$(this).attr('timeed','true');
				var did = Math.floor(Math.random()*78090000);
				$(this).attr('id',did);
				setTimeout("vvMsgClose("+did+")",1000);
			}
        });
		$(document).trigger('vvUpload');
		$(document).trigger('vvInit');   
		vvTrackEvent('upload-image',"upload.website.image");
	}else{
		$(".qq-upload-status-text").html(data.msg);			   
	}
};
function vvFileUploader(div,action,vvUPText,handel){
	if(handel == undefined){
		handel = handleComplete;	
	}
	var ext = div.attr('ext');	
	var allowedEx = ext.split(',');// new Array('jpg','png','gif');
	
	var nr = parseInt(div.attr('nr'));
	if(vvUPText.uploadButtonE == undefined){	
		vvUPText.uploadButtonE = vvUPText.uploadButton;
	}	 
	vvUPText.uploadButton = vvUPText.uploadButtonE.replace('{nr}',nr+1); 
	 
	var uploader = new qq.FineUploader({
		element:div.get(0),
		debug: debugg,
		multiple :true,
		validation :{
			sizeLimit: 50*1024*1024, 
			allowedExtensions:allowedEx
		},
		request: {
			params:{},
			endpoint: path+"?init="+action+"&div_id="+div.parent().attr('id')+"&action="+div.attr('action')
		},
		callbacks: {
			onComplete:handleComplete,
			onSubmit: function(){				 
				 
			},
			onCancel:function(){
				
			}
		},
		text: vvUPText
	});	
}
  
var userHandleComplete = function( id, fileName, data) {
	if(data.success){		  
		//Visa alert om klar
		//vvSendingImage = true;
		file = data.vv.file;
		nr = $("#file_list ._item").length+1;
		
		$("#file_list").append('<div class="_item msg">Fil '+nr+' - '+file.name+'.'+file.ext+' <a href="#" class=" submit option parms_alter right delete-'+file.id+' confirm" style="right:auto;float:right;position:static;" action="vvDeleteChallangeUploadFile" parms_key="idx" parms_val="'+file.id+'" confirm="Vill du ta bort?" idx="delete-'+file.id+'">Ta bort</a></div>');
		 
		$(document).trigger('vvInit');  
		$(document).trigger('vvUpload');  
		vvTrackEvent('upload-image',"upload.website.file");
	}else{
		$(".qq-upload-status-text").html(data.msg);			   
	}
};  
function vvUserUploader(div,action,vvUPText,multi){
	
	if(multi == "" || multi == "false"){
		multi = false;
	}
	 
	var ext = $("#"+div).attr('ext');	
	var allowedEx = ext.split(',');// new Array('jpg','png','gif');	 
	var uploader = new qq.FineUploader({
		element: document.getElementById(div),
		debug: debugg,
		multiple :multi,
		validation :{
			sizeLimit: 50*1024*1024, 
			allowedExtensions:allowedEx
		},
		request: {
			params:{},
			endpoint: path+"?init="+action
		},
		callbacks: {
			onComplete:userHandleComplete,
			onSubmit: function(){				 
				 
			},
			onCancel:function(){
				
			}
		},
		text: vvUPText
	});
} 
  

  
  
  
/* ---------------------------------- VV SUBMIT FORM --------------- */

function vvSubmitForm(oobForm){
	  	vvDebugg('vvSubmitForm depricated');
		return;
}


function vvMsgClose(id){

	$("."+id).slideToggle('slow',function(){
		parent = $(this).parent();
		$(this).remove();
		
		if(parent.children().length==0){
			parent.fadeOut();
		}
	});

}
var msgBoxTimeout = null;
function vvMsgBox(msg,bind,time,type){
	if(type == undefined){
		type = "info";
	}
	
	if(time == undefined){
		time = 3000;
	}
	var di = Math.floor(Math.random()*10000);
	var i = 0;
	if(bind != undefined){
		if(bind != null){
			$(document).unbind(bind);
		}
	}
	$(".msgbox").each(function(index, element) {
		i++;
		var did = di+'_'+i;
		var $msg = $('<div class="alert alert-'+type+' msg '+did+'" role="alert" style="display:none;">'+msg+'</div>');
        $(this).fadeIn().append($msg);
		$msg.slideToggle('fast');
		setTimeout("vvMsgClose('"+did+"')",time);
		
		if(bind != undefined){
			if(bind != null){
				$(document).bind(bind,{ id: did },function(event){
					var data = event.data;	
					 	
					$("."+data.id).slideToggle('slow',function(){
						$(this).remove();
					});			 
				});
			}
		}
    });
	
	
	
	
}

function vvConsole(message){
	 level = false;
	if(debugg){
		if (window.console) {
			if (!level || level === 'info') {
				window.console.log(message);
			}
			else
			{
				if (window.console[level]) {
					window.console[level](message);
				}
				else {
					window.console.log('<' + level + '> ' + message);
				}
			}
		}
	}
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}	
function removeSpaces(string) {
	return string.split(' ').join('');
}
 
function sortItemsByVal(listItem,listDiv,searchClass){ 
	 
	 
	if($(listDiv).attr('invert') == 1){
		invertIs = 0;
		$(listDiv).attr('invert',0);
	}else{
		invertIs = 1;
		$(listDiv).attr('invert',1);
	}	
	$items = $(listDiv).find(listItem);
	$items_sort = new Array; 
	var i = 0;
	$items.each(function(){		 
		$items_sort[i] = new Array();
		$items_sort[i]['val'] = removeSpaces($(this).find("."+searchClass).text()); 
		$items_sort[i]['html'] =  $(this).clone();
		i++;
	});
	$items_sort.sort(function(a,b){
		if(!isNaN(a['val']) && !isNaN(b['val'])){			
			var aT =  parseFloat(a['val']);
			var bT = parseFloat(b['val']);
			if(invertIs == 1){
				return aT - bT;
			}else{
				return bT - aT;
			}			 
		}else{			
			var aT =  a['val'].toLowerCase();
			var bT = b['val'].toLowerCase();			
			if(invertIs == 1){
				return ((aT < bT) ? -1 : ((aT > bT) ? 1 : 0));
			}else{
				return ((bT < aT) ? -1 : ((bT > aT) ? 1 : 0));
			}	
		}	
	});
	$(listDiv).html('');
	for(var e = 0;e<$items_sort.length;e++){		
		$(listDiv).append($items_sort[e]['html']);
	};
	
}







	
function HtmlDecode(s) {
	if(s == "" || s == undefined){
		return;
	}
 s=s.replace(/&#43;/gi,'+');
 return s.replace(/&[a-z]+;/gi, function(entity) {
  switch (entity) {
  case '&quot;': return String.fromCharCode(0x0022);
  case '&amp;': return String.fromCharCode(0x0026);
  case '&lt;': return String.fromCharCode(0x003c);
  case '&gt;': return String.fromCharCode(0x003e);
  case '&nbsp;': return String.fromCharCode(0x00a0);
  case '&iexcl;': return String.fromCharCode(0x00a1);
  case '&cent;': return String.fromCharCode(0x00a2);
  case '&pound;': return String.fromCharCode(0x00a3);
  case '&curren;': return String.fromCharCode(0x00a4);
  case '&yen;': return String.fromCharCode(0x00a5);
  case '&brvbar;': return String.fromCharCode(0x00a6);
  case '&sect;': return String.fromCharCode(0x00a7);
  case '&uml;': return String.fromCharCode(0x00a8);
  case '&copy;': return String.fromCharCode(0x00a9);
  case '&ordf;': return String.fromCharCode(0x00aa);
  case '&laquo;': return String.fromCharCode(0x00ab);
  case '&not;': return String.fromCharCode(0x00ac);
  case '&shy;': return String.fromCharCode(0x00ad);
  case '&reg;': return String.fromCharCode(0x00ae);
  case '&macr;': return String.fromCharCode(0x00af);
  case '&deg;': return String.fromCharCode(0x00b0);
  case '&plusmn;': return String.fromCharCode(0x00b1);
  case '&sup2;': return String.fromCharCode(0x00b2);
  case '&sup3;': return String.fromCharCode(0x00b3);
  case '&acute;': return String.fromCharCode(0x00b4);
  case '&micro;': return String.fromCharCode(0x00b5);
  case '&para;': return String.fromCharCode(0x00b6);
  case '&middot;': return String.fromCharCode(0x00b7);
  case '&cedil;': return String.fromCharCode(0x00b8);
  case '&sup1;': return String.fromCharCode(0x00b9);
  case '&ordm;': return String.fromCharCode(0x00ba);
  case '&raquo;': return String.fromCharCode(0x00bb);
  case '&frac14;': return String.fromCharCode(0x00bc);
  case '&frac12;': return String.fromCharCode(0x00bd);
  case '&frac34;': return String.fromCharCode(0x00be);
  case '&iquest;': return String.fromCharCode(0x00bf);
  case '&Agrave;': return String.fromCharCode(0x00c0);
  case '&Aacute;': return String.fromCharCode(0x00c1);
  case '&Acirc;': return String.fromCharCode(0x00c2);
  case '&Atilde;': return String.fromCharCode(0x00c3);
  case '&Auml;': return String.fromCharCode(0x00c4);
  case '&Aring;': return String.fromCharCode(0x00c5);
  case '&AElig;': return String.fromCharCode(0x00c6);
  case '&Ccedil;': return String.fromCharCode(0x00c7);
  case '&Egrave;': return String.fromCharCode(0x00c8);
  case '&Eacute;': return String.fromCharCode(0x00c9);
  case '&Ecirc;': return String.fromCharCode(0x00ca);
  case '&Euml;': return String.fromCharCode(0x00cb);
  case '&Igrave;': return String.fromCharCode(0x00cc);
  case '&Iacute;': return String.fromCharCode(0x00cd);
  case '&Icirc;': return String.fromCharCode(0x00ce);
  case '&Iuml;': return String.fromCharCode(0x00cf);
  case '&ETH;': return String.fromCharCode(0x00d0);
  case '&Ntilde;': return String.fromCharCode(0x00d1);
  case '&Ograve;': return String.fromCharCode(0x00d2);
  case '&Oacute;': return String.fromCharCode(0x00d3);
  case '&Ocirc;': return String.fromCharCode(0x00d4);
  case '&Otilde;': return String.fromCharCode(0x00d5);
  case '&Ouml;': return String.fromCharCode(0x00d6);
  case '&times;': return String.fromCharCode(0x00d7);
  case '&Oslash;': return String.fromCharCode(0x00d8);
  case '&Ugrave;': return String.fromCharCode(0x00d9);
  case '&Uacute;': return String.fromCharCode(0x00da);
  case '&Ucirc;': return String.fromCharCode(0x00db);
  case '&Uuml;': return String.fromCharCode(0x00dc);
  case '&Yacute;': return String.fromCharCode(0x00dd);
  case '&THORN;': return String.fromCharCode(0x00de);
  case '&szlig;': return String.fromCharCode(0x00df);
  case '&agrave;': return String.fromCharCode(0x00e0);
  case '&aacute;': return String.fromCharCode(0x00e1);
  case '&acirc;': return String.fromCharCode(0x00e2);
  case '&atilde;': return String.fromCharCode(0x00e3);
  case '&auml;': return String.fromCharCode(0x00e4);
  case '&aring;': return String.fromCharCode(0x00e5);
  case '&aelig;': return String.fromCharCode(0x00e6);
  case '&ccedil;': return String.fromCharCode(0x00e7);
  case '&egrave;': return String.fromCharCode(0x00e8);
  case '&eacute;': return String.fromCharCode(0x00e9);
  case '&ecirc;': return String.fromCharCode(0x00ea);
  case '&euml;': return String.fromCharCode(0x00eb);
  case '&igrave;': return String.fromCharCode(0x00ec);
  case '&iacute;': return String.fromCharCode(0x00ed);
  case '&icirc;': return String.fromCharCode(0x00ee);
  case '&iuml;': return String.fromCharCode(0x00ef);
  case '&eth;': return String.fromCharCode(0x00f0);
  case '&ntilde;': return String.fromCharCode(0x00f1);
  case '&ograve;': return String.fromCharCode(0x00f2);
  case '&oacute;': return String.fromCharCode(0x00f3);
  case '&ocirc;': return String.fromCharCode(0x00f4);
  case '&otilde;': return String.fromCharCode(0x00f5);
  case '&ouml;': return String.fromCharCode(0x00f6);
  case '&divide;': return String.fromCharCode(0x00f7);
  case '&oslash;': return String.fromCharCode(0x00f8);
  case '&ugrave;': return String.fromCharCode(0x00f9);
  case '&uacute;': return String.fromCharCode(0x00fa);
  case '&ucirc;': return String.fromCharCode(0x00fb);
  case '&uuml;': return String.fromCharCode(0x00fc);
  case '&yacute;': return String.fromCharCode(0x00fd);
  case '&thorn;': return String.fromCharCode(0x00fe);
  case '&yuml;': return String.fromCharCode(0x00ff);
  case '&OElig;': return String.fromCharCode(0x0152);
  case '&oelig;': return String.fromCharCode(0x0153);
  case '&Scaron;': return String.fromCharCode(0x0160);
  case '&scaron;': return String.fromCharCode(0x0161);
  case '&Yuml;': return String.fromCharCode(0x0178);
  case '&fnof;': return String.fromCharCode(0x0192);
  case '&circ;': return String.fromCharCode(0x02c6);
  case '&tilde;': return String.fromCharCode(0x02dc);
  case '&Alpha;': return String.fromCharCode(0x0391);
  case '&Beta;': return String.fromCharCode(0x0392);
  case '&Gamma;': return String.fromCharCode(0x0393);
  case '&Delta;': return String.fromCharCode(0x0394);
  case '&Epsilon;': return String.fromCharCode(0x0395);
  case '&Zeta;': return String.fromCharCode(0x0396);
  case '&Eta;': return String.fromCharCode(0x0397);
  case '&Theta;': return String.fromCharCode(0x0398);
  case '&Iota;': return String.fromCharCode(0x0399);
  case '&Kappa;': return String.fromCharCode(0x039a);
  case '&Lambda;': return String.fromCharCode(0x039b);
  case '&Mu;': return String.fromCharCode(0x039c);
  case '&Nu;': return String.fromCharCode(0x039d);
  case '&Xi;': return String.fromCharCode(0x039e);
  case '&Omicron;': return String.fromCharCode(0x039f);
  case '&Pi;': return String.fromCharCode(0x03a0);
  case '& Rho ;': return String.fromCharCode(0x03a1);
  case '&Sigma;': return String.fromCharCode(0x03a3);
  case '&Tau;': return String.fromCharCode(0x03a4);
  case '&Upsilon;': return String.fromCharCode(0x03a5);
  case '&Phi;': return String.fromCharCode(0x03a6);
  case '&Chi;': return String.fromCharCode(0x03a7);
  case '&Psi;': return String.fromCharCode(0x03a8);
  case '&Omega;': return String.fromCharCode(0x03a9);
  case '&alpha;': return String.fromCharCode(0x03b1);
  case '&beta;': return String.fromCharCode(0x03b2);
  case '&gamma;': return String.fromCharCode(0x03b3);
  case '&delta;': return String.fromCharCode(0x03b4);
  case '&epsilon;': return String.fromCharCode(0x03b5);
  case '&zeta;': return String.fromCharCode(0x03b6);
  case '&eta;': return String.fromCharCode(0x03b7);
  case '&theta;': return String.fromCharCode(0x03b8);
  case '&iota;': return String.fromCharCode(0x03b9);
  case '&kappa;': return String.fromCharCode(0x03ba);
  case '&lambda;': return String.fromCharCode(0x03bb);
  case '&mu;': return String.fromCharCode(0x03bc);
  case '&nu;': return String.fromCharCode(0x03bd);
  case '&xi;': return String.fromCharCode(0x03be);
  case '&omicron;': return String.fromCharCode(0x03bf);
  case '&pi;': return String.fromCharCode(0x03c0);
  case '&rho;': return String.fromCharCode(0x03c1);
  case '&sigmaf;': return String.fromCharCode(0x03c2);
  case '&sigma;': return String.fromCharCode(0x03c3);
  case '&tau;': return String.fromCharCode(0x03c4);
  case '&upsilon;': return String.fromCharCode(0x03c5);
  case '&phi;': return String.fromCharCode(0x03c6);
  case '&chi;': return String.fromCharCode(0x03c7);
  case '&psi;': return String.fromCharCode(0x03c8);
  case '&omega;': return String.fromCharCode(0x03c9);
  case '&thetasym;': return String.fromCharCode(0x03d1);
  case '&upsih;': return String.fromCharCode(0x03d2);
  case '&piv;': return String.fromCharCode(0x03d6);
  case '&ensp;': return String.fromCharCode(0x2002);
  case '&emsp;': return String.fromCharCode(0x2003);
  case '&thinsp;': return String.fromCharCode(0x2009);
  case '&zwnj;': return String.fromCharCode(0x200c);
  case '&zwj;': return String.fromCharCode(0x200d);
  case '&lrm;': return String.fromCharCode(0x200e);
  case '&rlm;': return String.fromCharCode(0x200f);
  case '&ndash;': return String.fromCharCode(0x2013);
  case '&mdash;': return String.fromCharCode(0x2014);
  case '&lsquo;': return String.fromCharCode(0x2018);
  case '&rsquo;': return String.fromCharCode(0x2019);
  case '&sbquo;': return String.fromCharCode(0x201a);
  case '&ldquo;': return String.fromCharCode(0x201c);
  case '&rdquo;': return String.fromCharCode(0x201d);
  case '&bdquo;': return String.fromCharCode(0x201e);
  case '&dagger;': return String.fromCharCode(0x2020);
  case '&Dagger;': return String.fromCharCode(0x2021);
  case '&bull;': return String.fromCharCode(0x2022);
  case '&hellip;': return String.fromCharCode(0x2026);
  case '&permil;': return String.fromCharCode(0x2030);
  case '&prime;': return String.fromCharCode(0x2032);
  case '&Prime;': return String.fromCharCode(0x2033);
  case '&lsaquo;': return String.fromCharCode(0x2039);
  case '&rsaquo;': return String.fromCharCode(0x203a);
  case '&oline;': return String.fromCharCode(0x203e);
  case '&frasl;': return String.fromCharCode(0x2044);
  case '&euro;': return String.fromCharCode(0x20ac);
  case '&image;': return String.fromCharCode(0x2111);
  case '&weierp;': return String.fromCharCode(0x2118);
  case '&real;': return String.fromCharCode(0x211c);
  case '&trade;': return String.fromCharCode(0x2122);
  case '&alefsym;': return String.fromCharCode(0x2135);
  case '&larr;': return String.fromCharCode(0x2190);
  case '&uarr;': return String.fromCharCode(0x2191);
  case '&rarr;': return String.fromCharCode(0x2192);
  case '&darr;': return String.fromCharCode(0x2193);
  case '&harr;': return String.fromCharCode(0x2194);
  case '&crarr;': return String.fromCharCode(0x21b5);
  case '&lArr;': return String.fromCharCode(0x21d0);
  case '&uArr;': return String.fromCharCode(0x21d1);
  case '&rArr;': return String.fromCharCode(0x21d2);
  case '&dArr;': return String.fromCharCode(0x21d3);
  case '&hArr;': return String.fromCharCode(0x21d4);
  case '&forall;': return String.fromCharCode(0x2200);
  case '&part;': return String.fromCharCode(0x2202);
  case '&exist;': return String.fromCharCode(0x2203);
  case '&empty;': return String.fromCharCode(0x2205);
  case '&nabla;': return String.fromCharCode(0x2207);
  case '&isin;': return String.fromCharCode(0x2208);
  case '&notin;': return String.fromCharCode(0x2209);
  case '&ni;': return String.fromCharCode(0x220b);
  case '&prod;': return String.fromCharCode(0x220f);
  case '&sum;': return String.fromCharCode(0x2211);
  case '&minus;': return String.fromCharCode(0x2212);
  case '&lowast;': return String.fromCharCode(0x2217);
  case '&radic;': return String.fromCharCode(0x221a);
  case '&prop;': return String.fromCharCode(0x221d);
  case '&infin;': return String.fromCharCode(0x221e);
  case '&ang;': return String.fromCharCode(0x2220);
  case '&and;': return String.fromCharCode(0x2227);
  case '&or;': return String.fromCharCode(0x2228);
  case '&cap;': return String.fromCharCode(0x2229);
  case '&cup;': return String.fromCharCode(0x222a);
  case '&int;': return String.fromCharCode(0x222b);
  case '&there4;': return String.fromCharCode(0x2234);
  case '&sim;': return String.fromCharCode(0x223c);
  case '&cong;': return String.fromCharCode(0x2245);
  case '&asymp;': return String.fromCharCode(0x2248);
  case '&ne;': return String.fromCharCode(0x2260);
  case '&equiv;': return String.fromCharCode(0x2261);
  case '&le;': return String.fromCharCode(0x2264);
  case '&ge;': return String.fromCharCode(0x2265);
  case '&sub;': return String.fromCharCode(0x2282);
  case '&sup;': return String.fromCharCode(0x2283);
  case '&nsub;': return String.fromCharCode(0x2284);
  case '&sube;': return String.fromCharCode(0x2286);
  case '&supe;': return String.fromCharCode(0x2287);
  case '&oplus;': return String.fromCharCode(0x2295);
  case '&otimes;': return String.fromCharCode(0x2297);
  case '&perp;': return String.fromCharCode(0x22a5);
  case '&sdot;': return String.fromCharCode(0x22c5);
  case '&lceil;': return String.fromCharCode(0x2308);
  case '&rceil;': return String.fromCharCode(0x2309);
  case '&lfloor;': return String.fromCharCode(0x230a);
  case '&rfloor;': return String.fromCharCode(0x230b);
  case '&lang;': return String.fromCharCode(0x2329);
  case '&rang;': return String.fromCharCode(0x232a);
  case '&loz;': return String.fromCharCode(0x25ca);
  case '&spades;': return String.fromCharCode(0x2660);
  case '&clubs;': return String.fromCharCode(0x2663);
  case '&hearts;': return String.fromCharCode(0x2665);
  case '&diams;': return String.fromCharCode(0x2666);
  default: return '';
  }
 })
}
	
	
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

